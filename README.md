# Angular Docs App
## Screenshot
---
![Visit Angular Docs App](./preview/Home.png)
![Visit Angular Docs App](./preview/WithNavbar.png)
![Visit Angular Docs App](./preview/withDigram.png)
![Visit Angular Docs App](./preview/WithCode.png)

---
## 😎 Features
### → App on Angular 10
### → regular documentation update
### → easy language
### → block diagram , </>source code preview
### → angular code Run functionality
### → modular application
### → Json server use for HTTP Client request (CURD)
### → what's new angular versions
---
## Clone repository
### step 1
```
npm install -g json-server
```
### step 2 - set this json in db.json
```json
{
  "users": [
    {
      "id": "1",
      "name": "Hitesh",
      "lastname": "Rajput",
      "email": "Hitesh00@gmail.com",
      "language": "java script"
    },
    {
      "id": "2",
      "name": "rajesh",
      "lastname": "yadav",
      "email": "raj123@gmail.com",
      "language": "android"
    },
    {
      "id": "3",
      "name": "raj",
      "lastname": "roy",
      "email": "royId23@gmail.com",
      "language": "java"
    }
  ]
}
```
### step 3
```
json-server --watch db.json
```

### step 4 - run angular app
```
ng s --open
```
---
## 📃 Topics
### → INTRODUCTION & SETUP
### Introduction 
#### Set-Up 
#### Structure 
#### Booting Process
<br>

###  → MODULE 
#### What Is Module 
#### Create Module 
#### Child & Parent Module
<br>

###  → COMPONENT
#### What Is Component
#### Create Component
#### @Component Decorator
<br>

###  → DIRECTIVE
#### What-Is-Directive
#### Component Driective
#### Structural Driective(*ngIf , else ,then, *ngFor ,[ngSwitch])
#### Attribute ([ngStyle] , [ngClass])
<br>

### →  DECORATORS
### What Is Decorator
### Class Decorators
### Property Decorators
### Parameter Decorators
### Method Decorators
<br>

### →  DEPENDENCY INJECTION
#### What Is Dependency Injection
#### Services
<br>

### →  DATA BINDING
####  One Way Data Binding
####  Two Way Data Binding
<br>

### →  PIPES
#### What Is Pipes
#### Built In Pipes
#### Custom Pipes
<br>

### →  ROUTING
#### What Is Routing
#### Router
#### Router Outlet
#### Router Link
#### How To Do Routing
#### Empty Path
#### Redirective Router
#### Wild Card Route
#### Child Route
#### Auxiliary Route
<br>

###  → FORM
#### What Is Form
#### Template Form
#### Reactive Form
#### Nexted React Form
#### Form Array
<br>

###  → FORM VALIDATION
#### What Is Form Validation
#### Template Form Validation
#### Reactive Form Validation
<br>

###  → MODULE LOADING
#### Eager Loading
#### Lazy-Loading
#### Pre Loading
#### Custom Pre Loading
#### Feature Modules
<br>

###  → OBSERVABLES & RXJS
#### What Is Observables
#### Subscribe Web API
#### Async Pipe
#### Async With Ngif
#### Rxjs METHOD
<br>

###  → HTTP CLIENT
#### What Is Http Client
#### Get
#### Post
#### Put
#### Delete
#### HTTP error handling
<br>

###  → LIFE CYCLE HOOKS
#### What Is Life Cycle
#### Constructor
#### Life Cycle Stages
<br>

###  → DESUGARS MICRO & TEMPLATE SYNTAX
#### Template Reference Variables
#### ng-Template
#### ng-Container
<br>

### → COMPONENT INTERACTION
#### What Is Component Interaction
#### @ViewChild()
#### @ViewChild using Directive 
#### @ViewChild Using Template Variable
#### @Input & @Output Decorator
#### Dynamic Component Loader
<br>

### → SESSION MANAGEMENT
#### What Is Session
#### Local Storage
#### Session Storage
#### Some Method & Checking
<br>

### → COOKIES
#### What Is Cookies
#### Setup Cookies