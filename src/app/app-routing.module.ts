import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { AngularDashboardModule } from './angular-dashboard/angular-dashboard.module'
const routes: Routes = [
  { path: "", component: HomeComponent },
  { path: "angular", loadChildren: () => import('./angular-dashboard/angular-dashboard.module').then(m => m.AngularDashboardModule) }
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
