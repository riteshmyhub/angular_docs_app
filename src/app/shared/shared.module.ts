import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from "@angular/forms";
import { ReactiveFormsModule } from '@angular/forms';
const all_module = [
    FormsModule,
    ReactiveFormsModule,
]
@NgModule({
  imports: [
    all_module,
    CommonModule
  ],
  exports:[all_module]
})
export class SharedModule { }
