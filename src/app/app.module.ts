import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { from } from 'rxjs';
import { HomeComponent } from './home/home.component';
import { AngularDashboardComponent } from './angular-dashboard/angular-dashboard.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AngularDashboardComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
