import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SessionManagementComponent } from './session-management.component';
import { WhatIsSessionComponent } from './what-is-session/what-is-session.component';
import { LocalStorageComponent } from './types/local-storage/local-storage.component';
import { SessionStorageComponent } from './types/session-storage/session-storage.component';
import { MethodAndCheckingComponent } from './method-and-checking/method-and-checking.component';

const routes: Routes = [
  {path:'',component:SessionManagementComponent,children:[
    {path:'',component:WhatIsSessionComponent},
    {path:'what-is-session',component:WhatIsSessionComponent},
    {path:'local-storage',component:LocalStorageComponent},
    {path:'session-storage',component:SessionStorageComponent},
    {path:'some-method',component:MethodAndCheckingComponent},
  ]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SessionManagementRoutingModule { }
