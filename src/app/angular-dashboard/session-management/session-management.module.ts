import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SessionManagementRoutingModule } from './session-management-routing.module';
import { WhatIsSessionComponent } from './what-is-session/what-is-session.component';
import { LocalStorageComponent } from './types/local-storage/local-storage.component';
import { SessionStorageComponent } from './types/session-storage/session-storage.component';
import { MethodAndCheckingComponent } from './method-and-checking/method-and-checking.component';


@NgModule({
  declarations: [WhatIsSessionComponent, LocalStorageComponent, SessionStorageComponent, MethodAndCheckingComponent],
  imports: [
    CommonModule,
    SessionManagementRoutingModule
  ]
})
export class SessionManagementModule { }
