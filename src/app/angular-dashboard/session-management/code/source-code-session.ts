export const SessionList = [
    { id: 1, point: 'what is session', path: 'what-is-session' },
    { id: 2, point: 'local storage', path: 'local-storage' },
    { id: 3, point: 'session storage', path: 'session-storage' },
    { id: 4, point: 'some method & checking', path: 'some-method' },
]
export const sessionCode = {
    jsonObj:`
    mydata= [
        {name:'raj',subject:'vuejs'},
        {name:'rajesh',subject:'angular'},
        {name:'rajendr',subject:'react'},
      ],
      ngOnInit(){
        localStorage.setItem('localData',JSON.stringify(this.mydata)); 
      }`,
     checkStore:{
         c1_ts:`
         localStorage.setItem('localData','this data from local data update');
         localStorage.clear()`,
         c2_ts:`
         localStorage.getItem('localData')

         if (localStorage.length > 0) {
            alert('data available')
          } else {
            console.log('data no available');
          }`,
     },
      checkSupport:{
          c2_ts:`
          if (window.localStorage) {
              console.log('is support !');
              
          } else {
            console.log('is not support !');
          }`,
      }, 
}