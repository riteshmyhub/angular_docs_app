import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MethodAndCheckingComponent } from './method-and-checking.component';

describe('MethodAndCheckingComponent', () => {
  let component: MethodAndCheckingComponent;
  let fixture: ComponentFixture<MethodAndCheckingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MethodAndCheckingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MethodAndCheckingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
