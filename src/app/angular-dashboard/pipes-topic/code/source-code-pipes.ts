export const PipesList = [
    { id: 1, point: 'what is pipes', path: 'what-is-pipes' },
    { id: 2, point: 'built in', path: 'builtIn' },
    { id: 3, point: 'custom pipes', path: 'customPipes' },
]
export const pipesCode = {
    syntax: `
    {{ variable | pipes name}}

    examlpe -
    {{ name | uppercase}}`,
    type: {
        builin: {
            para: {
                syntax: `
            {{ variable | pipes name:parameter}}`,
            },
            someParaPipes: {
                // data pipe
                Date: {
                    html: `
            <p>without using pipes date : {{DOB}}</p>

            <p>Using pipes for date : {{DOB | date}}</p>

            <p>Using pipes for date formate : {{DOB | date: 'dd-MM-yyyy'}}</p>

            <p>Using pipes for time: {{DOB | date: 'mediumTime'}}</p>`,
                    ts: `
                    DOB = new Date (2020,8,2);`,
                },
                // currency pipes
                currency: {
                    html: `
            <p>{{salary | currency : 'EUR' }}</p> 
            <p>{{salary | currency : 'INR'}}</p> 
            <p>{{salary | currency : 'CNY'}}</p>`,
                    ts: `
                    salary:number= 80000;`,
                },
                // chaining pipes
                chaining:{
                    syntax:`
                    {{variable | pipe1 | pipe2| pipe3 .............pipes(n)}}`,
                    withPipe:`
                    {{variable | pipe1:'para1':para2| pipe2:'para1':para2|pipe3:'para1':para2 .............pipes(n)}}`,
                    ex:`
                    <h3>{{DOB | date:'medium' | uppercase}}</h3>`,
                },
                string:{
                    uppercase:{
                        syntax:`
                        <p>{{name | uppercase}}</p>`, 
                    },
                    lowercase:{
                        syntax:`
                        <p>{{name | lowercase}}</p>`,
                    },
                    slice:{
                        syntax:`
             <h3>text in slice  : {{name | slice: 2}}</h3>
             <h3>text in slice  : {{name | slice: 3:5}}</h3>`,
                    },
                },
                decimal:{
                    syntax:`
                    <h3>{{num | number: "8.2-4"}}</h3>`,
                },
                Jsonpipes:{
                    syntax:`
                    {{ collectionName | json }}`,
                    withSlice:{
                        syntax:`
                        {{ collectionName | slice: 1:4 | json }}`,
                    },
                },
            },
        },
        custom:{
            syntax:`
            import { Pipe, PipeTransform } from '@angular/core';

         @Pipe({
           name: 'custom'//name of pipe
         })
         export class CustomPipe implements PipeTransform {

           transform(value: enter data types , oject of key name:enter data types):str,number,boolean etc{
             
             #any codition 
            return 'text'+value;
           }

         }`,
         transform:{
           ts:`
           data_person: any[] = [
            { id: 1, patient: 'raj', status: 'postive', action: '' },
            { id: 2, patient: 'rajesh', status: 'negative', action: '' },
            { id: 3, patient: 'raju', status: 'postive', action: '' },
            { id: 4, patient: 'rajendr', status: 'negative', action: '' },
            { id: 5, patient: 'raja', status: 'postive', action: '' },
          ]`,
           pipe_ts:`
           import { Pipe, PipeTransform } from '@angular/core';

           @Pipe({
             name: 'custom'//pipe name
           })
           export class CustomPipe implements PipeTransform {
           
             transform(value: string, status: string): any {
               if (status == 'postive') {
                 return 'please quarantine in your home' + value;
               } else if (status == 'negative') {
                 return 'take care' + value;
               }
             }
           
           }`,
           html:`
           <table class="table table-hover">
           <thead>
             <tr>
               <th scope="col">id</th>
               <th scope="col">patient</th>
               <th scope="col">status</th>
               <th scope="col">action</th>
             </tr>
           </thead>
           <tbody>
             <tr *ngFor="let covid of data_person">
               <td>{{ covid.id }}</td>
               <td>{{ covid.patient }}</td>
               <td>{{ covid.status }}</td>
               <td>{{ covid.action | custom:covid.status}}</td>
             </tr>
           </tbody>
         </table>`,
         },

        }
    },
}