import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PipesTopicRoutingModule } from './pipes-topic-routing.module';
import { WhatIsPipesComponent } from './what-is-pipes/what-is-pipes.component';
import { BuiltInPipesComponent } from './types/built-in-pipes/built-in-pipes.component';
import { CustomPipesComponent } from './types/custom-pipes/custom-pipes.component';
import { CustomPipe } from './types/custom-pipes/pipes/custom.pipe';


@NgModule({
  declarations: [WhatIsPipesComponent, BuiltInPipesComponent, CustomPipesComponent, CustomPipe],
  imports: [
    CommonModule,
    PipesTopicRoutingModule
  ]
})
export class PipesTopicModule { }
