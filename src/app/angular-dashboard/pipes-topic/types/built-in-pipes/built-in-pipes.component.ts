import { Component, OnInit } from '@angular/core';
import { pipesCode } from '../../code/source-code-pipes';

@Component({
  selector: 'app-built-in-pipes',
  templateUrl: './built-in-pipes.component.html',
  styleUrls: ['./built-in-pipes.component.css']
})
export class BuiltInPipesComponent implements OnInit {
  pipesCode = pipesCode;
  // data
  DOB = new Date(2020, 8, 2);
  // currency
  salary: number = 80000;
  // uppercase
  name: string = "text holder";
  // decimal
  num: number = 0.44646;
  constructor() { }
  // pipes with Json
  JpList: any[] = [
    { id: 1, product: 'milk' },
    { id: 2, product: 'vag' },
    { id: 3, product: 'non-vag' },
    { id: 4, product: 'other' },
  ];
  ngOnInit(): void {
  }

}
