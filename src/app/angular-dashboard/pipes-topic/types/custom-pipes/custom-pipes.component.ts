import { Component, OnInit } from '@angular/core';
import { pipesCode } from '../../code/source-code-pipes';

@Component({
  selector: 'app-custom-pipes',
  templateUrl: './custom-pipes.component.html',
  styleUrls: ['./custom-pipes.component.css']
})
export class CustomPipesComponent implements OnInit {
  pipesCode = pipesCode;
  constructor() { }
  // for custom pipes 

  data_person: any[] = [
    { id: 1, patient: 'raj', status: 'postive', action: '' },
    { id: 2, patient: 'rajesh', status: 'negative', action: '' },
    { id: 3, patient: 'raju', status: 'postive', action: '' },
    { id: 4, patient: 'rajendr', status: 'negative', action: '' },
    { id: 5, patient: 'raja', status: 'postive', action: '' },
  ]
  ngOnInit(): void {
  }

}
