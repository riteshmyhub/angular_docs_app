import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'custom'//pipe name
})
export class CustomPipe implements PipeTransform {

  transform(value: string, status: string): any {
    if (status == 'postive') {
      return 'please quarantine in your home' + value;
    } else if (status == 'negative') {
      return 'take care' + value;
    }
  }

}
