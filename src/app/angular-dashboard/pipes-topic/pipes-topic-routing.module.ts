import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PipesTopicComponent } from './pipes-topic.component';
import { WhatIsPipesComponent } from './what-is-pipes/what-is-pipes.component';
import { BuiltInPipesComponent } from './types/built-in-pipes/built-in-pipes.component';
import { CustomPipesComponent } from './types/custom-pipes/custom-pipes.component';

const routes: Routes = [
  {path:'',component:PipesTopicComponent,children:[
    {path:'',component:WhatIsPipesComponent},
    {path:'what-is-pipes',component:WhatIsPipesComponent},
    {path:'builtIn',component:BuiltInPipesComponent},
    {path:'customPipes',component:CustomPipesComponent}
  ]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PipesTopicRoutingModule { }
