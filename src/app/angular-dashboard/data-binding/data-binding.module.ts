import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from "@angular/forms";

import { DataBindingRoutingModule } from './data-binding-routing.module';
import { WhatIsDataBindingComponent } from './what-is-data-binding/what-is-data-binding.component';
import { OneWayComponent } from "./types/one-way/one-way.component";
import { TwoWayComponent } from './types/two-way/two-way.component'


@NgModule({
  declarations: [WhatIsDataBindingComponent, OneWayComponent, TwoWayComponent],
  imports: [
    CommonModule,
    DataBindingRoutingModule,
    FormsModule
  ]
})
export class DataBindingModule { }
