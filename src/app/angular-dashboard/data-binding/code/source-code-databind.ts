export const DataBindList = [
    { id: 1, point: 'what is Data Binding', path: 'what-is-DataBinding' },
    { id: 2, point: 'one way', path: 'oneWey' },
    { id: 3, point: 'two way', path: 'twoWay' },
]

export const dataBinding = {
    oneway: {
        Interpolation: {
            html: `
            <h1>{{Interpolation}} , {{100+100}} ,{{methoot()}} </h1>`,
            ts: `
            Interpolation = "this text from Interpolation"`
        },
        property: {
            html: `
            <button [disabled]="isdisable">Button</button>
            <button [disabled]="!isdisable">Button</button> (so return false)`,
            ts: `
            isdisable: boolean = true;`,
        },
        attribute: {
            syntex: ``,
            html: `
             <img src="bird" alt="" [width]="imgWidth" srcset="">`,
            ts: `
             imgWidth = 200;`,
        },
        event: {
            html: ``,
            ts: ``,
        },
    },
    twoway: {
        eventAndPro: {
            syntax: `
            <input [value]= "variableName" (input)="variableName = $even.target.value" />`,
            html: `
            <input [value]="inputdata" (input)="inputdata = $event.target.value" /> 
             
             <span> text here : {{inputdata}}</span>`,
            ts: `
            inputdata:string = "";`,
        },
        ngModel: {
            appImport: `
            import {NgModule} from "@angular/core";
            import { FormsModule, ReactiveFormsModule } from "@angular/forms"`,
            ts: ``,
        },
    },

}