import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DataBindingComponent } from './data-binding.component';
import { WhatIsDataBindingComponent } from './what-is-data-binding/what-is-data-binding.component';
import { OneWayComponent } from './types/one-way/one-way.component';
import { TwoWayComponent } from './types/two-way/two-way.component';

const routes: Routes = [
  {path:'',component:DataBindingComponent, children:[
    {path:'',component:WhatIsDataBindingComponent},
    {path:'what-is-DataBinding',component:WhatIsDataBindingComponent},
    {path:'oneWey',component:OneWayComponent},
    {path:'twoWay',component:TwoWayComponent},
  ]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DataBindingRoutingModule { }
