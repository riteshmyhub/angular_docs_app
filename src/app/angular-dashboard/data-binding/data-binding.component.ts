import { Component, OnInit } from '@angular/core';
import { DataBindList } from "./code/source-code-databind"
@Component({
  selector: 'app-data-binding',
  templateUrl: './data-binding.component.html',
  styleUrls: ['./data-binding.component.css']
})
export class DataBindingComponent implements OnInit {
  DataBindList = DataBindList;
  constructor() { }

  ngOnInit(): void {
  }

}
