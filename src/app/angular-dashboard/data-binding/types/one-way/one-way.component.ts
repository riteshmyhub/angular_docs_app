import { Component, OnInit } from '@angular/core';
import { dataBinding } from '../../code/source-code-databind';

@Component({
  selector: 'app-one-way',
  templateUrl: './one-way.component.html',
  styleUrls: ['./one-way.component.css']
})
export class OneWayComponent implements OnInit {
  dataBinding=dataBinding;
  Interpolation = "this text from Interpolation"
  isdisable: boolean = true;

  // attr bind
  imgWidth = 200;
  constructor() { }
  InterP() {
    return 'this text from Interpolation using method()'
  }
  eventBind() {
    alert('this is event binding');
  }
  ngOnInit(): void {
  }

}
