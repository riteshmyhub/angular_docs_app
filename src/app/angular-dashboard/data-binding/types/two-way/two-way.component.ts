import { Component, OnInit } from '@angular/core';
import { dataBinding } from '../../code/source-code-databind';

@Component({
  selector: 'app-two-way',
  templateUrl: './two-way.component.html',
  styleUrls: ['./two-way.component.css']
})
export class TwoWayComponent implements OnInit {
  dataBinding = dataBinding
  constructor() { }
  inputdata: string = "";
  ngModelEx: string = "";
  ngOnInit(): void {
  }

}
