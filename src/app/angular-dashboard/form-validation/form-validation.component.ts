import { Component, OnInit } from '@angular/core';
import { FormValidationList } from './code/source-code-formValidation';
@Component({
  selector: 'app-form-validation',
  templateUrl: './form-validation.component.html',
  styleUrls: ['./form-validation.component.css']
})
export class FormValidationComponent implements OnInit {
  FormValidationList = FormValidationList;
  constructor() { }

  ngOnInit(): void {
  }

}
