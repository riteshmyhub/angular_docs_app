import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormValidationRoutingModule } from './form-validation-routing.module';
import { WhatIsFVComponent } from './what-is-fv/what-is-fv.component';
import { TemplateValidationComponent } from './types/template-validation/template-validation.component';
import { ReactiveValidationComponent } from './types/reactive-validation/reactive-validation.component';
import { SharedModule } from 'src/app/shared/shared.module';


@NgModule({
  declarations: [WhatIsFVComponent, TemplateValidationComponent, ReactiveValidationComponent],
  imports: [
    CommonModule,
    FormValidationRoutingModule,
    SharedModule
  ]
})
export class FormValidationModule { }
