import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormValidationComponent } from './form-validation.component';
import { WhatIsFVComponent } from './what-is-fv/what-is-fv.component';
import { ReactiveValidationComponent } from './types/reactive-validation/reactive-validation.component';
import { TemplateValidationComponent } from './types/template-validation/template-validation.component';

const routes: Routes = [
  {path:'',component:FormValidationComponent, children:[
    {path:'',component:WhatIsFVComponent},
    {path:'what-is-Form-Validation',component:WhatIsFVComponent},
    {path:'Template-Validation',component:TemplateValidationComponent},
    {path:'Reactiv-Validation',component:ReactiveValidationComponent},
  ]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FormValidationRoutingModule { }
