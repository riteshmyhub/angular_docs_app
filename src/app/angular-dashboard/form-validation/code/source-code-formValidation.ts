export const FormValidationList = [
    { id: 1, point: 'what is Form Validation', path: 'what-is-Form-Validation' },
    { id: 2, point: 'Template Validation', path: 'Template-Validation' },
    { id: 3, point: 'Reactiv Validation', path: 'Reactiv-Validation' },
]
export const FormValidCode = {
    tempValid:{
         moduleTs:`
         import { FormsModule } from '@angular/forms'`,
         ts:`
         output;
         SubMethood(a) {
           console.log(JSON.stringify(a.value));
           this.output = a.value;
         }`,
         css:`
         input.ng-invalid {
            border-left: 6px solid red;
          }
          input.ng-valid {
            border-left: 6px solid green;
          }`,
         html:`
         <form #myform="ngForm" (ngSubmit)="SubMethood(myform)">

               <span>name</span>
               <input type="text" name="name" #name="ngModel" required ngModel />
               <span *ngIf="name.invalid && name.touched" class="error">* you not enter name or name is invalid
               </span>

               <span>password</span>
                <input minlength="5" maxlength="10" type="password" name="password" #password="ngModel" required ngModel />
                <span *ngIf="password.invalid && password.touched" class="error">
                * you not enter password or password is invalid </span>

               <button [disabled]="myform.invalid">Submit</button>
               <h4>{{output | json}}</h4>
           </form>`,
    },
    reactValid:{
      moduleTs:`
         import { FormsModule } from '@angular/forms'
         import { ReactiveFormsModule } from '@angular/forms';`,
      ts:`
         import {FormBuilder, FormControl, MinLengthValidator, PatternValidator, FormArray,} from "@angular/forms";

         import { Validators } from "@angular/forms";

         myform;
         constructor(private formB: FormBuilder) { }
         output;
         FormSubmit() {
           this.output = this.myform.value;
         }
        // all vaildation pattern
        regex = {
          name: /^[a-zA-Z]{3,17}$/,
          lastname: /^[a-zA-Z]{3,17}$/,
          password: /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{8,16}$/,
          moblie: /^[0-9]{10,12}$/,
        };
        // all input vaildation
        firstname_validators = [
          Validators.required,
          Validators.pattern(this.regex.name),
        ];
        lastname_validators = [
          Validators.required,
          Validators.pattern(this.regex.lastname),
        ];
        password_validators = [
          Validators.required,
          Validators.pattern(this.regex.password),
        ];
        moblie_validators = [
          Validators.required,
          Validators.pattern(this.regex.moblie),
        ];
        ngOnInit() {
          this.myform = this.formB.group({
            firstname: ["", this.firstname_validators],
            lastname: ["", this.lastname_validators],
            password: ["", this.password_validators],
            moblie: ["", this.moblie_validators]
          });
        }
        // for error message
        get firstname() {
          return this.myform.get("firstname");
        }
        get lastname() {
          return this.myform.get("lastname");
        }
        get password() {
          return this.myform.get("password");
        }
        get moblie() {
          return this.myform.get("moblie");
        }`,
        css:`
        input.ng-invalid {
           border-left: 6px solid red;
         }
         input.ng-valid {
           border-left: 6px solid green;
         }`,
        html:`
        <form [formGroup]="myform" (ngSubmit)="FormSubmit()">
        
          <span>name</span><br />
          <input type="text" formControlName="firstname" />
          <span class="error" *ngIf="firstname.invalid && firstname.touched">* invalid name</span>
        
          <span>last name</span><br />
          <input type="text" formControlName="lastname" />
          <span class="error" *ngIf="lastname.invalid && lastname.touched">* invalid last name</span>
        
          <span>password</span><br />
          <input type="password" formControlName="password" />
          <span class="error" *ngIf="password.invalid && password.touched">* 
          invalid password (use capital small ,letter ,number & special character & length - &lt; min 8 , &gt;max 16)</span>
        
          <span>moblie</span><br />
          <input type="text" formControlName="moblie" />
          <span class="error"  *ngIf="moblie.invalid && moblie.touched">* invalid moblie number</span>
        
          <button type="submit" [disabled]="myform.invalid">Submit now</button>
        {{output | json}}
      </form>`,
    },
}