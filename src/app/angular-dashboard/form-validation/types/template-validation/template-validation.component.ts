import { Component, OnInit } from '@angular/core';
import { FormValidCode } from '../../code/source-code-formValidation';

@Component({
  selector: 'app-template-validation',
  templateUrl: './template-validation.component.html',
  styleUrls: ['./template-validation.component.css']
})
export class TemplateValidationComponent implements OnInit {
  FormValidCode = FormValidCode;
  constructor() { }
  output;
  SubMethood(a) {
    console.log(JSON.stringify(a.value));
    this.output = a.value;
  }
  ngOnInit(): void {
  }

}
