import { Component, OnInit } from '@angular/core';
import { FormValidCode } from '../../code/source-code-formValidation';
import {FormBuilder, FormControl, MinLengthValidator, PatternValidator, FormArray,} from "@angular/forms";
import { Validators } from "@angular/forms";
@Component({
  selector: 'app-reactive-validation',
  templateUrl: './reactive-validation.component.html',
  styleUrls: ['./reactive-validation.component.css']
})
export class ReactiveValidationComponent implements OnInit {
  FormValidCode =FormValidCode;

  
  myform;
  constructor(private formB: FormBuilder) { }
  output;
  FormSubmit() {
    this.output = this.myform.value;
  }
 // all vaildation pattern
 regex = {
   name: /^[a-zA-Z]{3,17}$/,
   lastname: /^[a-zA-Z]{3,17}$/,
   password: /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{8,16}$/,
   moblie: /^[0-9]{10,12}$/,
 };
 // all input vaildation
 firstname_validators = [
   Validators.required,
   Validators.pattern(this.regex.name),
 ];
 lastname_validators = [
   Validators.required,
   Validators.pattern(this.regex.lastname),
 ];
 password_validators = [
   Validators.required,
   Validators.pattern(this.regex.password),
 ];
 moblie_validators = [
   Validators.required,
   Validators.pattern(this.regex.moblie),
 ];
 ngOnInit() {
   this.myform = this.formB.group({
     firstname: ["", this.firstname_validators],
     lastname: ["", this.lastname_validators],
     password: ["", this.password_validators],
     moblie: ["", this.moblie_validators]
   });
 }
 // for error message
 get firstname() {
   return this.myform.get("firstname");
 }
 get lastname() {
   return this.myform.get("lastname");
 }
 get password() {
   return this.myform.get("password");
 }
 get moblie() {
   return this.myform.get("moblie");
 }

}
