import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AngularDashboardRoutingModule } from './angular-dashboard-routing.module';
import { IntroductionComponent } from './introduction/introduction.component';
import { ModuleTopicComponent } from './module-topic/module-topic.component';
import { DirectiveComponent } from './directive/directive.component';
import { ComponentsComponent } from './components/components.component';
import { DataBindingComponent } from './data-binding/data-binding.component';
import { PipesTopicComponent } from './pipes-topic/pipes-topic.component';
import { SessionManagementComponent } from './session-management/session-management.component';
import { RoutingTopicComponent } from './routing-topic/routing-topic.component';
import { FormComponent } from './form/form.component';
import { DesugarsMicrosyntaxComponent } from './desugars-microsyntax/desugars-microsyntax.component';
import { NgTemplateComponent } from './desugars-microsyntax/types/ng-template/ng-template.component';
import { ComponentInteractionComponent } from './component-interaction/component-interaction.component';
import { FormValidationComponent } from './form-validation/form-validation.component';
import { ModuleLoadingComponent } from './module-loading/module-loading.component';
import { DecoratorsTopicComponent } from './decorators-topic/decorators-topic.component';
import { ObservablesAndRxjsComponent } from './observables-and-rxjs/observables-and-rxjs.component';
import { HttpClientTopicComponent } from './http-client-topic/http-client-topic.component'
import { from } from 'rxjs';
import { CookiesTopicComponent } from './cookies-topic/cookies-topic.component';
import { LifeCycleAndHooksComponent } from './life-cycle-and-hooks/life-cycle-and-hooks.component';
import { DependencyInjectionComponent } from './dependency-injection/dependency-injection.component';


@NgModule({
  declarations: [IntroductionComponent, ModuleTopicComponent, DirectiveComponent, ComponentsComponent, DataBindingComponent, PipesTopicComponent, SessionManagementComponent, RoutingTopicComponent, FormComponent, DesugarsMicrosyntaxComponent, NgTemplateComponent, ComponentInteractionComponent, FormValidationComponent, ModuleLoadingComponent, DecoratorsTopicComponent, ObservablesAndRxjsComponent, HttpClientTopicComponent, CookiesTopicComponent, LifeCycleAndHooksComponent, DependencyInjectionComponent],
  imports: [
    CommonModule,
    AngularDashboardRoutingModule
  ]
})
export class AngularDashboardModule { }
