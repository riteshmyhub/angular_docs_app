import { Component, OnInit } from '@angular/core';
import { DecoratorList } from './code/source-code-decorators';
@Component({
  selector: 'app-decorators-topic',
  templateUrl: './decorators-topic.component.html',
  styleUrls: ['./decorators-topic.component.css']
})
export class DecoratorsTopicComponent implements OnInit {
  DecoratorList = DecoratorList;
  constructor() { }

  ngOnInit(): void {
  }

}
