import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DecoratorsTopicComponent } from './decorators-topic.component';
import { WhatIsDecoratorComponent } from './what-is-decorator/what-is-decorator.component';
import { DecoClassComponent } from './types/deco-class/deco-class.component';
import { DecoMethodComponent } from './types/deco-method/deco-method.component';
import { DecoParameterComponent } from './types/deco-parameter/deco-parameter.component';
import { DecoPropertyComponent } from './types/deco-property/deco-property.component';

const routes: Routes = [
  {path:'',component:DecoratorsTopicComponent, children:[
    {path:'',component:WhatIsDecoratorComponent},
    {path:'what-is-Decorator',component:WhatIsDecoratorComponent},
    {path:'Class-Decorator',component:DecoClassComponent},
    {path:'Property-Decorator',component:DecoPropertyComponent},
    {path:'Parameter-Decorator',component:DecoParameterComponent},
    {path:'Method-Decorator',component:DecoMethodComponent},
  ]},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DecoratorsTopicRoutingModule { }
