import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DecoratorsTopicRoutingModule } from './decorators-topic-routing.module';
import { WhatIsDecoratorComponent } from './what-is-decorator/what-is-decorator.component';
import { DecoMethodComponent } from './types/deco-method/deco-method.component';
import { DecoClassComponent } from './types/deco-class/deco-class.component';
import { DecoPropertyComponent } from './types/deco-property/deco-property.component';
import { DecoParameterComponent } from './types/deco-parameter/deco-parameter.component';


@NgModule({
  declarations: [WhatIsDecoratorComponent, DecoMethodComponent, DecoClassComponent, DecoPropertyComponent, DecoParameterComponent],
  imports: [
    CommonModule,
    DecoratorsTopicRoutingModule
  ]
})
export class DecoratorsTopicModule { }
