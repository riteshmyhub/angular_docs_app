import { Component, OnInit ,HostListener } from '@angular/core';
import { decoratorCode } from '../../code/source-code-decorators';

@Component({
  selector: 'app-deco-method',
  templateUrl: './deco-method.component.html',
  styleUrls: ['./deco-method.component.css']
})
export class DecoMethodComponent implements OnInit {
  decoratorCode =decoratorCode;
  constructor() { }

  ngOnInit(): void {
  }
   @HostListener('click') method(){
      alert('HostListener working')
   }
}
