import { Component, OnInit } from '@angular/core';
import { decoratorCode } from '../../code/source-code-decorators';

@Component({
  selector: 'app-deco-parameter',
  templateUrl: './deco-parameter.component.html',
  styleUrls: ['./deco-parameter.component.css']
})
export class DecoParameterComponent implements OnInit {
  decoratorCode=decoratorCode;
  constructor() { }

  ngOnInit(): void {
  }

}
