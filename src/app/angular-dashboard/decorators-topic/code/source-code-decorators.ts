export const DecoratorList = [
    { id: 1, point: 'what is Decorator', path: 'what-is-Decorator' },
    { id: 2, point: 'Class Decorator', path: 'Class-Decorator' },
    { id: 3, point: 'Property Decorator', path: 'Property-Decorator' },
    { id: 3, point: 'Parameter Decorator', path: 'Parameter-Decorator' },
    { id: 3, point: 'Method Decorator', path: 'Method-Decorator' },
]

export const decoratorCode = {
    Method:{
        ts:`
        import {HostListener } from '@angular/core'; 

        @HostListener('click') method(){
            alert('HostListener working')
         }`,
    },
    Parameter:{
        services:{
            service:`
            import { Injectable } from '@angular/core';

            @Injectable({
                providedIn: 'root'
            })
            export class MyServiceService {
    
              constructor() { }
            }`,
            appMode:`
            import { MyServiceService } from './decorator-topic/my-service.service';
            @NgModule({
              declarations: [],
              imports: [],
              providers:[MyServiceService]
            })
            export class HomeModule {}`,
            ts:`
            import { Component , Inject } from "@angular/core";
            import { MyServiceService } from "./my-service.service";
    
            @Component({
              selector: "app-decorator-topic",
              templateUrl: "./decorator-topic.component.html",
              styleUrls: ["./decorator-topic.component.css"],
            })
    
            export class DecoratorTopicComponent implements OnInit {
              constructor(@Inject(MyServiceService) mys) {
                console.log(mys);
              }
            }`,
        }
    }  
}