import { Component, OnInit } from '@angular/core';
import { List } from './nav-list';

@Component({
  selector: 'app-angular-dashboard',
  templateUrl: './angular-dashboard.component.html',
  styleUrls: ['./angular-dashboard.component.css']
})
export class AngularDashboardComponent implements OnInit {

  TopicHeading: any = '';
  List = List;//all topic list
  SideNav: boolean = false;
  constructor() { }

  open(pram) {
    this.SideNav = pram;
  }
  closenav(pram){
    this.SideNav = pram;
  }
  close(pram, num) {
    this.SideNav = pram;
    return this.TopicHeading = this.List[num].topic;
  }
  ngOnInit(): void {
  }
}
