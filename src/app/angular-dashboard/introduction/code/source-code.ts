export const Topicpoint = [
    { id: 1, point: 'Introduction', path: 'intro' },
    { id: 2, point: 'SetUp', path: 'SetUp' },
    { id: 3, point: 'Structure', path: 'Structure' },
    { id: 3, point: 'Booting', path: 'Booting' },
]
//---------------------source Code-----------------------
const stepOne = `
     import { enableProdMode } from '@angular/core';
     import { platformBrowserDynamic } from '@angularplatform-browser-dynamic';

     import { AppModule } from './app/app.module';
     import { environment } from './environments/environment';

     if (environment.production) {
  enableProdMode();
     }

     platformBrowserDynamic().bootstrapModule(AppModule)
       .catch(err => console.error(err));`;

const steptwo = `
     import { AppRoutingModule } from "./app-routing.module";
     import { AppComponent } from "./app.component";

     @NgModule({
       declarations: [AppComponent],
       imports: [
         BrowserModule,
         AppRoutingModule,
       ],
       providers: [],
       bootstrap: [AppComponent],
     })
     export class AppModule {}`;

const stepthree = `
     import { Component } from '@angular/core';
     
     @Component({
       selector: 'app-root',
       templateUrl: './app.component.html',
       styleUrls: ['./app.component.css']
     })
     export class AppComponent {
       title = 'mode';
     }`;

const stepfour = `
     <!doctype html>
     <html lang="en">
     <head>
         <meta charset="utf-8">
       <title>Mode</title>
       <base href="/">
       <meta name="viewport" content="width=device-width,initial-scale=1">
       <link rel="icon" type="image/x-icon" href="favicon.ico">
     </head>
     <body>
       <app-root></app-root>
     </body>
     </html>`;

export const intro = { stepOne, steptwo, stepthree, stepfour }     