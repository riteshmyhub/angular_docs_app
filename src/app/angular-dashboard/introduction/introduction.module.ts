import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { IntroductionRoutingModule } from './introduction-routing.module';
import { SetUpComponent } from "./set-up/set-up.component";
import { BootingComponent } from "./booting/booting.component";
import { StructureComponent } from "./structure/structure.component";
import { IntroComponent } from './intro/intro.component';
import { HighlightService } from "../../prism/highlight.service";
@NgModule({
  declarations: [
    SetUpComponent,
    BootingComponent,
    StructureComponent,
    IntroComponent
  ],
  imports: [
    CommonModule,
    IntroductionRoutingModule
  ],
  providers: [HighlightService]
})
export class IntroductionModule { }
