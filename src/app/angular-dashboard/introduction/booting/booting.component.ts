import { Component, OnInit } from '@angular/core';
import { intro } from '../code/source-code';
import { HighlightService } from 'src/app/prism/highlight.service';

@Component({
  selector: 'app-booting',
  templateUrl: './booting.component.html',
  styleUrls: ['./booting.component.css']
})
export class BootingComponent implements OnInit {
  intro = intro;
  highlighted: boolean = false;
  constructor(private highlightService: HighlightService) { }

  ngOnInit(): void {
  }
  ngAfterViewChecked() {
    this.highlightService.highlightAll();
    this.highlighted = true;
  }

}
