import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SetUpComponent } from "./set-up/set-up.component";
import { BootingComponent } from "./booting/booting.component";
import { StructureComponent } from "./structure/structure.component";
import { IntroductionComponent } from './introduction.component';
import {IntroComponent} from "./intro/intro.component"

const routes: Routes = [
  { path: '', component: IntroductionComponent , children:[
    {path:'', redirectTo:'intro'},
    {path:'intro',component:IntroComponent},
    {path:'SetUp',component:SetUpComponent},
    {path:'Booting',component:BootingComponent},
    {path:'Structure',component:StructureComponent},
  ]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class IntroductionRoutingModule { }
