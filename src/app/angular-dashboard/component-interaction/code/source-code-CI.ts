export const C_interaction_list = [
  { id: 1, point: 'Component Interaction ?', path: 'what-is-Component-Interaction' },
  { id: 2, point: '@ViewChild', path: 'ViewChild' },
  { id: 3, point: '@ViewChild Using Directive', path: 'View-Child-Using-Directive' },
  { id: 3, point: '@ViewChild Using Template Variable', path: 'View-Child-Template-Variable' },
  { id: 3, point: '@Input & @Output', path: 'Input_Output-Decorator' },
  { id: 3, point: 'Dynamic component loader', path: 'Dynamic-component-loader' },
]

export const C_interaction = {
  ViewChild: {
    C_C: {
      html: `
            <h2>i am child component : {{name}}</h2>`,
      ts: `
            export class DemoComponent {
               name = 'hello ';
               incri() {
                   this.name = 'angular';
                 }
            }`,
    },
    P_C: {
      html: `
            <button (click)="Parent_method()">click here</button>
             <app-demo></app-demo>`,
      ts: `
            @ViewChild(DemoComponent) xyz: DemoComponent;// here put child class name

            Parent_method() {
              this.xyz.Child_method()
            }`,
    },
  },
  UsingDirective: {
    Directive_ts: `
    import { Directive, ElementRef, AfterViewInit } from '@angular/core';

     @Directive({
     selector: '[appDVC]'
     })
     export class DVCDirective implements AfterViewInit {

     constructor(private eleR: ElementRef) { }
     ngAfterViewInit() {
         this.eleR.nativeElement.style.color = 'red';
     }
     changeColor(inputColor: string) {
         this.eleR.nativeElement.style.color = inputColor;
     }
     }`,
    html: `
     <h2 appDVC>Hello , Angular</h2>
      <input type="radio" name="CB" (click)="selectColor('yellow')" />
      <input type="radio" name="CB" (click)="selectColor('blue')" />
      <input type="radio" name="CB" (click)="selectColor('gray')" />`,
    ts: `
      @ViewChild(DVCDirective) xyz: DVCDirective;
      constructor() { }
       selectColor(inputColor:string){
         this.xyz.changeColor(inputColor)
        }`,
  },
  UsingTemplate: {
    html: `
        <div #name >
            <h2>Hello , Template variable</h2>
        </div>`,
    ts: `
        import { Component, OnInit, ViewChild, ElementRef, AfterViewInit } from '@angular/core';

        export class ViewChildTemplateVarComponent implements AfterViewInit {

         @ViewChild('name') xyx: ElementRef;
         ngAfterViewInit() {
            this.xyx.nativeElement.style.backgroundColor = '#61DAFB';
            this.xyx.nativeElement.style.padding = '10px';
           }
        }`,
  },
  DynamicComponent: {
    appMode: `
        @NgModule({
            declarations: [C1Component, C2Component],
            imports: [],
            entryComponents:[C1Component, C2Component]
          })`,
    ts: `
        import { DyOneComponent } from '../dy-one/dy-one.component';
        import { DyTwoComponent } from '../dy-two/dy-two.component';

        dummyComp = DyTwoComponent;
         constructor() { }
         animalName(compo) {
           if (compo === 'cow') {
             this.dummyComp = DyOneComponent;
           }
           else if (compo === 'dog') {
             this.dummyComp = DyTwoComponent;
           }
         }`,
    html: `
      <button (click)="animalName('cow')"> Component One</button>
      <button (click)="animalName('dog')"> Component Two</button>
      <h2>
        <ng-container *ngComponentOutlet="dummyComp"></ng-container>
      </h2>`,
  },
  Ip_Op_decorator: {
    Ip: {
      child_ts: `
      import { Input } from '@angular/core';

      @Input() byParent: string;`,
      child_html: `
      ask by parent : {{ byParent }}`,
      parent_ts: `
      ask_prenet = 'who is the Prime Minister of India ?';`,
      parent_html: `
      <app-child-comp [byParent]="ask_prenet"></app-child-comp>`,
    },
    Op: {
      child_ts: `
      import { Output, EventEmitter } from '@angular/core';

      @Output() reply: EventEmitter<string> = new EventEmitter();
      Answer = 'Narendra Modi'
      replyMehod() {
        this.reply.emit(this.Answer)
      }`,
      child_html: `
      <button (click)="replyMehod()">click to send </button>`,
      parent_ts: `
      answer
       getdata(param) {
         this.answer = param;
       }`,
      parent_html: `
      {{answer}}
      <app-child-comp (reply)="getdata($event)"></app-child-comp>`,
    }

  }
}