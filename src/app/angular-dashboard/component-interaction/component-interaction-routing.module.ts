import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ComponentInteractionComponent } from './component-interaction.component';
import { ViewChildComponent } from './types/view-child/view-child.component';
import {WhatIsCIComponent} from './what-is-ci/what-is-ci.component';
import { from } from 'rxjs';
import { DirectiveViewChildComponent } from './types/directive-view-child/directive-view-child.component';
import { ViewChildTemplateVarComponent } from './types/view-child-template-var/view-child-template-var.component';
import { DyComponentLoaderComponent } from './types/dy-component-loader/dy-component-loader.component';
import { InputOutputDecoComponent } from './types/Input_Output/input-output-deco/input-output-deco.component';

const routes: Routes = [
  {path:'',component:ComponentInteractionComponent,children:[
    {path:'',component:WhatIsCIComponent},
    {path:'what-is-Component-Interaction',component:WhatIsCIComponent},
    {path:'ViewChild',component:ViewChildComponent},
    {path:'View-Child-Using-Directive',component:DirectiveViewChildComponent},
    {path:'View-Child-Template-Variable',component:ViewChildTemplateVarComponent},
    {path:'Dynamic-component-loader',component:DyComponentLoaderComponent},
    {path:'Input_Output-Decorator',component:InputOutputDecoComponent},
  ]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ComponentInteractionRoutingModule { }
