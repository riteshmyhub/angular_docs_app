import { Component, OnInit } from '@angular/core';
import { C_interaction_list } from './code/source-code-CI';

@Component({
  selector: 'app-component-interaction',
  templateUrl: './component-interaction.component.html',
  styleUrls: ['./component-interaction.component.css']
})
export class ComponentInteractionComponent implements OnInit {
  C_interaction_list =C_interaction_list;
  constructor() { }

  ngOnInit(): void {
  }

}
