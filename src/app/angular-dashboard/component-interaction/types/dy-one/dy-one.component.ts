import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dy-one',
  template: `<h2>First component Loaded</h2>`,
})
export class DyOneComponent implements OnInit {
  constructor() { }

  ngOnInit(): void {
  }

}
