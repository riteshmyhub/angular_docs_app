import { Component, OnInit, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { C_interaction } from '../../code/source-code-CI';

@Component({
  selector: 'app-view-child-template-var',
  templateUrl: './view-child-template-var.component.html',
  styleUrls: ['./view-child-template-var.component.css']
})
export class ViewChildTemplateVarComponent implements AfterViewInit {
  C_interaction =C_interaction;
  @ViewChild('name') xyx: ElementRef;
  ngAfterViewInit() {
    this.xyx.nativeElement.style.backgroundColor = '#61DAFB';
    this.xyx.nativeElement.style.padding = '10px';
  }
  constructor() { }

  ngOnInit(): void {
  }

}
