import { Component, OnInit, ViewChild, ViewContainerRef, ComponentFactoryResolver } from '@angular/core';
import { DyOneComponent } from '../dy-one/dy-one.component';
import { DyTwoComponent } from '../dy-two/dy-two.component';
import { C_interaction } from '../../code/source-code-CI';

@Component({
  selector: 'app-dy-component-loader',
  templateUrl: './dy-component-loader.component.html',
  styleUrls: ['./dy-component-loader.component.css']
})
export class DyComponentLoaderComponent implements OnInit {
  C_interaction = C_interaction;

  dummyComp = DyTwoComponent;
  constructor() { }

  ngOnInit(): void {
  }
  animalName(compo) {
    if (compo === 'cow') {
      this.dummyComp = DyOneComponent;
    }
    else if (compo === 'dog') {
      this.dummyComp = DyTwoComponent;
    }
  }
}
