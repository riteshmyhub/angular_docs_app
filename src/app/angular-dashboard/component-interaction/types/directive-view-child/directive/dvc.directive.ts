import { Directive, ElementRef, AfterViewInit } from '@angular/core';

@Directive({
  selector: '[appDVC]'
})
export class DVCDirective implements AfterViewInit {

  constructor(private eleR: ElementRef) { }
  ngAfterViewInit() {
    this.eleR.nativeElement.style.color = 'red';
  }
  changeColor(inputColor: string) {
    this.eleR.nativeElement.style.color = inputColor;
  }
}
