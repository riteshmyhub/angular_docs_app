import { Component, OnInit, ViewChild } from '@angular/core';
import { DVCDirective } from './directive/dvc.directive'
import { from } from 'rxjs';
import { C_interaction } from '../../code/source-code-CI';
@Component({
  selector: 'app-directive-view-child',
  templateUrl: './directive-view-child.component.html',
  styleUrls: ['./directive-view-child.component.css']
})
export class DirectiveViewChildComponent implements OnInit {
// code 
C_interaction=C_interaction;

  @ViewChild(DVCDirective) xyz: DVCDirective;
  constructor() { }

  ngOnInit(): void {

  }
  selectColor(inputColor:string){
    this.xyz.changeColor(inputColor)
  }

}
