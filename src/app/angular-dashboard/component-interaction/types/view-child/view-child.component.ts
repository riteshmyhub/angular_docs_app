import { Component, OnInit, ViewChild } from '@angular/core';
import { DemoComponent } from './demo/demo.component'
import { from } from 'rxjs';
import { C_interaction } from '../../code/source-code-CI';
@Component({
  selector: 'app-view-child',
  templateUrl: './view-child.component.html',
  styleUrls: ['./view-child.component.css']
})
export class ViewChildComponent implements OnInit {
  // source code bind
  C_interaction=C_interaction;
  @ViewChild(DemoComponent) xyz: DemoComponent;

  P_Click() {
    this.xyz.incri()
  }
  constructor() { }

  ngOnInit(): void {
  }

}
