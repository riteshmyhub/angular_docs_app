import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-demo',
  template: `<h2>framework : {{name}}</h2>`,
})
export class DemoComponent implements OnInit {
  name = 'hello ';
  incri() {
    this.name = 'angular';
  }
  constructor() { }
  ngOnInit(): void {
  }

}
