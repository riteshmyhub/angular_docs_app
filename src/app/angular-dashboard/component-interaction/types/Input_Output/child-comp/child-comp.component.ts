import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-child-comp',
  templateUrl: './child-comp.component.html',
  styleUrls: ['./child-comp.component.css']
})
export class ChildCompComponent implements OnInit {
  //using @Input
  @Input() byParent: string;

  // //using @Output
  @Output() reply: EventEmitter<string> = new EventEmitter();
  Answer = 'Narendra Modi'
  constructor() { }
  replyMehod() {
    this.reply.emit(this.Answer)
  }
  ngOnInit(): void {

  }
}
