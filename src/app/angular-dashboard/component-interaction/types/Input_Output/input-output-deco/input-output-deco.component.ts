import { Component, OnInit } from '@angular/core';
import { C_interaction } from '../../../code/source-code-CI';

@Component({
  selector: 'app-input-output-deco',
  templateUrl: './input-output-deco.component.html',
  styleUrls: ['./input-output-deco.component.css']
})
export class InputOutputDecoComponent implements OnInit {
  C_interaction = C_interaction
  ask_prenet = 'who is the Prime Minister of India ?';
  constructor() { }
  answer
  ngOnInit(): void {
  }
  getdata(param) {
    this.answer = param;
  }
  // 
  InOp = false;
  switchBtn(param) {
    this.InOp = param;
  }
}
