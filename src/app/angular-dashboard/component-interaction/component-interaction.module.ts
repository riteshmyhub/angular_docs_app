import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ComponentInteractionRoutingModule } from './component-interaction-routing.module';
import { ViewChildComponent } from './types/view-child/view-child.component';
import { WhatIsCIComponent } from './what-is-ci/what-is-ci.component';
import { DemoComponent } from './types/view-child/demo/demo.component';
import { DirectiveViewChildComponent } from './types/directive-view-child/directive-view-child.component';

import { DVCDirective } from './types/directive-view-child/directive/dvc.directive';
import { ViewChildTemplateVarComponent } from './types/view-child-template-var/view-child-template-var.component';
import { DyComponentLoaderComponent } from './types/dy-component-loader/dy-component-loader.component';
import { DyOneComponent } from './types/dy-one/dy-one.component';
import { DyTwoComponent } from './types/dy-two/dy-two.component';
import { InputOutputDecoComponent } from './types/Input_Output/input-output-deco/input-output-deco.component';
import { ChildCompComponent } from './types/Input_Output/child-comp/child-comp.component';



@NgModule({
  declarations: [ViewChildComponent, WhatIsCIComponent, DemoComponent, DirectiveViewChildComponent, DVCDirective, ViewChildTemplateVarComponent, DyComponentLoaderComponent, DyOneComponent, DyTwoComponent, InputOutputDecoComponent, ChildCompComponent],
  imports: [
    CommonModule,
    ComponentInteractionRoutingModule
  ],
  entryComponents: [DyOneComponent, DyTwoComponent]
})
export class ComponentInteractionModule { }
