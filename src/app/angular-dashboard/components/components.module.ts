import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ComponentsRoutingModule } from './components-routing.module';
import { WhatIsComponentComponent } from './what-is-component/what-is-component.component';
import { GenerateComponentsComponent } from './generate-components/generate-components.component';
import { ComponentDecoratorComponent } from './component-decorator/component-decorator.component';


@NgModule({
  declarations: [WhatIsComponentComponent, GenerateComponentsComponent, ComponentDecoratorComponent],
  imports: [
    CommonModule,
    ComponentsRoutingModule
  ]
})
export class ComponentsModule { }
