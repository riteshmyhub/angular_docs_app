import { Component, OnInit } from '@angular/core';
import { component_code } from '../code/source-code-compo';

@Component({
  selector: 'app-generate-components',
  templateUrl: './generate-components.component.html',
  styleUrls: ['./generate-components.component.css']
})
export class GenerateComponentsComponent implements OnInit {
  component_code=component_code;
  constructor() { }

  ngOnInit(): void {
  }

}
