import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ComponentsComponent } from '../components/components.component';
import { WhatIsComponentComponent } from './what-is-component/what-is-component.component';
import { GenerateComponentsComponent } from './generate-components/generate-components.component';
import { ComponentDecoratorComponent } from './component-decorator/component-decorator.component';

const routes: Routes = [
  { path: "" , component: ComponentsComponent , children:[
    {path:"" ,component:WhatIsComponentComponent},
    {path:"what-is-component" ,component:WhatIsComponentComponent},
    {path:"create_Component" , component:GenerateComponentsComponent},
    {path:'component-decorator',component:ComponentDecoratorComponent},
  ]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ComponentsRoutingModule { }
