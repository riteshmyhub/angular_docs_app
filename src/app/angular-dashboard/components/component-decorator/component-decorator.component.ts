import { Component, OnInit } from '@angular/core';
import { component_code } from '../code/source-code-compo';

class Xyz {
  constructor() { }
  method() {
    console.log('i am Outer class');
  }
}
@Component({
  selector: 'app-component-decorator',
  templateUrl: './component-decorator.component.html',
  styleUrls: ['./component-decorator.component.css'],
  preserveWhitespaces: true,
  viewProviders: [Xyz]
})
export class ComponentDecoratorComponent implements OnInit {
  component_code = component_code
  constructor(public oueter: Xyz) { }

  ngOnInit(){
    this.oueter.method()
  }

}
