export const TopicComponents = [
  { id: 1, point: 'what is Components', path: 'what-is-component' },
  { id: 2, point: 'create Component', path: 'create_Component' },
  { id: 3, point: '@component(decorator)', path: 'component-decorator' },
]
export const component_code = {
  temp: `
      import { Component, OnInit } from '@angular/core';

  @Component({
    selector: 'app-name',
    templateUrl: './name.component.html',
    styleUrls: ['./name.component.css']
  })

  export class NameComponent implements OnInit {

    constructor() { }

    ngOnInit(): void {
    }
  }`,
  CompoDeco: {
    temp: {
      Template: `
      @Component({
        selector: 'app-name',
        template: backtick
               #here Html
          backtick,
        styleUrls: ['./name.component.css']
      })`,
      TemplateUrl: `
      @Component({
        selector: 'app-name',
        templateUrl: './name.component.html',
        styleUrls: ['./name.component.css']
      })`,
    },
    styleCss: {
      styles: `
      @Component({
        selector: 'app-name',
        templateUrl: './name.component.html',
        style:[
          'h2{color:black}',
          'div{width: 100%}'
        ]
      })`,
      styleUrl: `
      @Component({
        selector: 'app-name',
        templateUrl: './name.component.html',
        styleUrls: ['./name.component.css']
      })`,
    },
    Whitespaces:`
    @Component({
      selector: 'app-component-decorator',
      templateUrl: './component-decorator.component.html',
      styleUrls: ['./component-decorator.component.css'],
      preserveWhitespaces: true,
    })`,
    viewProviders:`
    class Xyz {
      constructor() { }
      method() {
        console.log('i am Outer class');
      }
    }
    @Component({
      selector: 'app-name',
      templateUrl: './name.component.html',
      styleUrls: ['./name.component.css'],
      viewProviders: [Xyz]
    })
    export class NameComponent implements OnInit {
      constructor(public oueter: Xyz) { }
    
      ngOnInit(){
        this.oueter.method()
      }
    
    }`,
  }
};

