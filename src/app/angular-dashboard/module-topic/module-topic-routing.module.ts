import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ModuleTopicComponent } from './module-topic.component';
import { WhatIsModuleComponent } from './what-is-module/what-is-module.component';
import { ChildAndParentComponent } from './child-and-parent/child-and-parent.component';
import { CreateModuleComponent } from './create-module/create-module.component';

const routes: Routes = [
  { path: "", component: ModuleTopicComponent ,children:[
    // child path 
    {path:'',component:WhatIsModuleComponent},
    {path:'what_in_module',component:WhatIsModuleComponent},
    {path:"child_&_parent",component:ChildAndParentComponent},
    {path:'create_module' ,component:CreateModuleComponent}
  ]} 
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ModuleTopicRoutingModule { }
