import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ModuleTopicRoutingModule } from './module-topic-routing.module';
import { WhatIsModuleComponent } from './what-is-module/what-is-module.component';
import { ChildAndParentComponent } from './child-and-parent/child-and-parent.component';
import { CreateModuleComponent } from './create-module/create-module.component';
import { HighlightService } from 'src/app/prism/highlight.service';


@NgModule({
  declarations: [WhatIsModuleComponent, ChildAndParentComponent, CreateModuleComponent],
  imports: [
    CommonModule,
    ModuleTopicRoutingModule
  ],
  providers: [HighlightService]
})
export class ModuleTopicModule { }
