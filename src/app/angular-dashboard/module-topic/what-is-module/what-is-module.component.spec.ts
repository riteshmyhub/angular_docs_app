import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WhatIsModuleComponent } from './what-is-module.component';

describe('WhatIsModuleComponent', () => {
  let component: WhatIsModuleComponent;
  let fixture: ComponentFixture<WhatIsModuleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WhatIsModuleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WhatIsModuleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
