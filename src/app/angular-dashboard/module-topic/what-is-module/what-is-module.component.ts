import { Component, OnInit } from '@angular/core';
import { CodeAppMode } from '../code/source.code';

@Component({
  selector: 'app-what-is-module',
  templateUrl: './what-is-module.component.html',
  styleUrls: ['./what-is-module.component.css']
})
export class WhatIsModuleComponent implements OnInit {
  CodeAppMode=CodeAppMode;
  constructor() { }

  ngOnInit(): void {
  }

}
