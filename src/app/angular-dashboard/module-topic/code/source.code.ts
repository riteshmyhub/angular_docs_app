export const Topicpoint = [
    { id: 1, point: 'What is module', path: 'what_in_module' },
    { id: 2, point: 'create module', path: 'create_module' },
    { id: 3, point: 'Child & Parent', path: 'child_&_parent' },
]
export const CodeAppMode = `
    import { BrowserModule } from "@angular/platform-browser";
    import { NgModule } from "@angular/core";

    import { AppRoutingModule } from "./app-routing.module";
    import { AppComponent } from "./app.component";

    @NgModule({
      declarations: [AppComponent],
      imports: [AppRoutingModule],   
      providers: [],
      bootstrap: [AppComponent],
    })
    export class AppModule {}`;