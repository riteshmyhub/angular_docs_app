import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChildAndParentComponent } from './child-and-parent.component';

describe('ChildAndParentComponent', () => {
  let component: ChildAndParentComponent;
  let fixture: ComponentFixture<ChildAndParentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChildAndParentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChildAndParentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
