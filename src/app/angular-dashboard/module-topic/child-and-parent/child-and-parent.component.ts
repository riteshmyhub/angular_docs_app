import { Component, OnInit } from '@angular/core';
import { HighlightService } from 'src/app/prism/highlight.service';

@Component({
  selector: 'app-child-and-parent',
  templateUrl: './child-and-parent.component.html',
  styleUrls: ['./child-and-parent.component.css']
})
export class ChildAndParentComponent implements OnInit {
  highlighted: boolean = false;
  constructor(private highlightService: HighlightService) { }

  ngOnInit(): void {
  }
  ngAfterViewChecked() {
    this.highlightService.highlightAll();
    this.highlighted = true;
  }
}
