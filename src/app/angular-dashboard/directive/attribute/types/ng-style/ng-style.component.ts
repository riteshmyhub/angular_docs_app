import { Component, OnInit } from '@angular/core';
import { D_Attribute } from '../../../code/source-code-directive';

@Component({
  selector: 'app-ng-style',
  templateUrl: './ng-style.component.html',
  styleUrls: ['./ng-style.component.css']
})
export class NgStyleComponent implements OnInit {
  D_Attribute = D_Attribute;
  constructor() { }
  // ng style
  ngStyleData: any[] = [
    { name: 'raj', result: 'fail' },
    { name: 'rajsh', result: 'pass' },
    { name: 'raju', result: 'distinction' },
  ];

  methood(result) {
    switch (result) {
      case 'fail':
        return "red";
      case 'pass':
        return "yellow";
      case 'distinction':
        return "green";
    }
  }
  ngOnInit(): void {
  }

}
