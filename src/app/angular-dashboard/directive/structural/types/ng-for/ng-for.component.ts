import { Component, OnInit } from '@angular/core';
import { D_Structural } from '../../../code/source-code-directive';

@Component({
  selector: 'app-ng-for',
  templateUrl: './ng-for.component.html',
  styleUrls: ['./ng-for.component.css']
})
export class NgForComponent implements OnInit {
  D_Structural = D_Structural;

  countries: any[] = [
    { name: 'Afghanistan' },
    { name: 'Argentina' },
    { name: 'Austria' },
    { name: 'Canada' },
    { name: 'Egypt' },
    { name: 'France' },
    { name: 'Germany' },
    { name: 'India' },
    { name: 'Japan' },
  ];
  // ngfor tarck by
  moblie: any[] = [
    { id: 1, name: 'sumsung' },
    { id: 2, name: 'htc' },
    { id: 3, name: 'vivo' },
  ]
  getmore(): void {
    this.moblie = [
      { id: 1, name: 'sumsung' },
      { id: 2, name: 'htc' },
      { id: 3, name: 'vivo' },
      { id: 4, name: 'lg' },
      { id: 5, name: 'mi' },
      { id: 6, name: 'oppo' },
      { id: 7, name: 'asus' },
      { id: 8, name: 'tenor' },
      { id: 9, name: 'oneplus' },
      { id: 10, name: 'nubia' },
    ];
  }

  TrackBymetod(index: string, moblie: any): string {
    return moblie.id;
  }
  // ngfor groups
  bank: any[] = [
    {name:'raj' , account:[
      {type:'Personal Account'},
      {type:'Real Account'},
      {type:'Nominal Account'},
    ]},
    {name:'rajesh' , account:[
      {type:'Personal Account'},
      {type:'Real Account'},
      {type:'Nominal Account'},
    ]},
  ];
  constructor() { }

  ngOnInit(): void {
  }

}
