import { Component, OnInit } from '@angular/core';
import { D_Structural } from '../../../code/source-code-directive';
@Component({
  selector: 'app-ng-switch',
  templateUrl: './ng-switch.component.html',
  styleUrls: ['./ng-switch.component.css']
})
export class NgSwitchComponent implements OnInit {
  D_Structural=D_Structural;
  constructor() { }
  xyz = '';
  method(props) {
      this.xyz = props.target.value;
      } 
  ngOnInit(): void {
  }

}
