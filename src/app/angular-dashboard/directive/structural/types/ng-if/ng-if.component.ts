import { Component, OnInit } from '@angular/core';
import { D_Structural } from '../../../code/source-code-directive';

@Component({
  selector: 'app-ng-if',
  templateUrl: './ng-if.component.html',
  styleUrls: ['./ng-if.component.css']
})
export class NgIfComponent implements OnInit {
  D_Structural=D_Structural
  constructor() { }
  abc = true;

  method(props) {
    this.abc = props;
  }
  // ng else
  if_var: boolean = true;
 
  Melse(props) {
    this.if_var = props;
  }
  // then
  then_var: boolean = true;
  Mthen(props) {
    this.then_var = props;
  }
  ngOnInit(): void {
  }

}
