import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DirectiveRoutingModule } from './directive-routing.module';
import { WhatIsDirectiveComponent } from './what-is-directive/what-is-directive.component';
import { CompoComponent } from './compo/compo.component';
import { AttributeComponent } from './attribute/attribute.component';
import { StructuralComponent } from './structural/structural.component';
import { NgIfComponent } from './structural/types/ng-if/ng-if.component';
import { NgForComponent } from './structural/types/ng-for/ng-for.component';
import { NgSwitchComponent } from './structural/types/ng-switch/ng-switch.component';
import { NgStyleComponent } from './attribute/types/ng-style/ng-style.component';
import { NgClassComponent } from './attribute/types/ng-class/ng-class.component';


@NgModule({
  declarations: [WhatIsDirectiveComponent, CompoComponent, AttributeComponent, StructuralComponent, NgIfComponent, NgForComponent, NgSwitchComponent, NgStyleComponent, NgClassComponent],
  imports: [
    CommonModule,
    DirectiveRoutingModule
  ]
})
export class DirectiveModule { }
