import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DirectiveComponent } from './directive.component';
import { WhatIsDirectiveComponent } from './what-is-directive/what-is-directive.component';
import { AttributeComponent } from './attribute/attribute.component';
import { StructuralComponent } from './structural/structural.component';
import { CompoComponent } from './compo/compo.component';
import { NgIfComponent } from './structural/types/ng-if/ng-if.component';
import { NgForComponent } from './structural/types/ng-for/ng-for.component';
import { NgSwitchComponent } from './structural/types/ng-switch/ng-switch.component';
import { NgStyle } from '@angular/common';
import { NgStyleComponent } from './attribute/types/ng-style/ng-style.component';
import { NgClassComponent } from './attribute/types/ng-class/ng-class.component';

const routes: Routes = [
  {
    path: "", component: DirectiveComponent, children: [
      { path: '', component: WhatIsDirectiveComponent },
      { path: 'what-is-directive', component: WhatIsDirectiveComponent },
      { path: "component", component: CompoComponent },
      { path: "",children:[
        {path:"structural",component: StructuralComponent},
       {path:"ngIf",component:NgIfComponent},
       {path:"ngFor",component:NgForComponent},
       {path:"ngSwitch",component:NgSwitchComponent},
      ]},
      { path: "", children:[
        {path:'attribute' ,component:AttributeComponent},
      ]},
      
      {path:'ngStyle',component:NgStyleComponent},
      {path:'ngClass',component:NgClassComponent},
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DirectiveRoutingModule { }
