export const Driective = [
  { id: 1, point: 'what is directive', path: 'what-is-directive' },
  { id: 2, point: 'component', path: 'component' },
  { id: 3, point: 'structural', path: 'structural' },
  { id: 4, point: 'attribute', path: 'attribute' },
]
// Component Driective-------
export const D_Component = {
  ts: `
    alertEvent() {
      alert("component directive working");
    }`,
  html: `
    <button (click)="alertEvent()">button</button>`,
}
export const D_Structural = {
  // _____________ng if _____________________
  ngif: {
    ngIf_t: `
      export class NameComponent implements OnInit {
         constructor() {}

         abc = true;

         method(props) {
           this.abc = props;
         }

         ngOnInit(): void {}
       }`,
    ngIf_h: `
       <button(click)="method(true)">Add</button>
       <button(click)="method(false)">Remove</button>

       <div *ngIf="abc" style="background-color: khaki;padding: 20px;">
           <h1>First Element</h1>
       </div>`,
    else_h: `
       <button (click)="method(true)">Show if part</button>
        <button (click)="method(false)">Show if part</button>

        <div *ngIf=" if_var; else else_var">
            <h1>this is if part</h1>
        </div>
        <ng-template #else_var >
            <div>
                <h1>this is eles part</h1>
            </div>
        </ng-template>`,
    else_t: `
        
        export class NameComponent implements OnInit {

          if_var: boolean = true;
 
          method(props) {
            this.if_var = props;
          }
          constructor() {}

          ngOnInit(): void {}
        }`,
    then_h: `
        <button (click)="method(true)">show then</button>
          <button (click)="method(false)">then else</button>

          <div *ngIf="then_var;then then_part; else else_part"></div>

          <ng-template #then_part>
              <h1>this is then part</h1>
          </ng-template>

          <ng-template #else_part>
              <h1>this is else part</h1>
          </ng-template>`,
    then_t: `
          export class NameComponent implements OnInit {

            then_var: boolean = true;
            method(props) {
              this.then_var = props;
            }
            constructor() {}

            ngOnInit(): void {}
          }`,
  },
  // _____________ng switch _____________________
  switch: {
    ts: `
     xyz = '';
     method(props) {
         this.xyz = props.target.value;
         }`,
    html: `
     
     <select (change)="method($event)">
     <option value="">select all option</option>
     <option value="a">part 1</option>
     <option value="b">part 2</option>
 </select>

  <div [ngSwitch]="xyz">
     <div *ngSwitchCase="'a'">
         <h1>option 1</h1>
     </div>
     <div *ngSwitchCase="'b'">
         <h1>option 2</h1>
     </div>
     <div *ngSwitchDefault>
         <h1>option Default</h1>
     </div>
  </div>`,
  },
  // _____________ng for _____________________
  ngForL: {
    html: `
    <div *ngFor="let country of countries">
        <h3>country name:{{ country.name }}</h3>
    </div>`,
    ts: `
    countries:any[]=[
      {name:'Afghanistan'},
      {name:'Argentina'},
      {name:'Austria'},
      {name:'Canada'},
      {name:'Egypt'},
      {name:'France'},
      {name:'Germany'},
      {name:'India'},
      {name:'Japan'},
    ];`,
    // ngFor lacal variable
    lacal_var: {
      index: `
      <div *ngFor="let country of countries; let i = index" >
          <h4>{{i}} {{country}}</h4>
      </div>`,
      first_last: `
      <div *ngFor="let country of countries; let F = first;let L = last">
          <h4>{{F}} {{country.name}} {{L}}</h4>
      </div>`,
      odd_even: `
      <div *ngFor="let country of countries; let O = odd;let E = even">
          <h4>{{O}} {{country.name}} {{E}}</h4>
      </div>`,
    },
    // ngFor track by
    track: {
      ts: `
      countries: any[] = [
        { id: 1, name: 'Afghanistan' },
        { id: 2, name: 'Argentina' },
        { id: 3, name: 'Austria' },
        { id: 5, name: 'Egypt' },
      ]
      getmore(): void {
        this.countries = [
          { id: 1, name: 'Afghanistan' },
          { id: 2, name: 'Argentina' },
          { id: 3, name: 'Austria' },
          { id: 5, name: 'Egypt' },
          { id: 6, name: 'France' },
          { id: 7, name: 'Germany' },
          { id: 8, name: 'India' },
          { id: 9, name: 'Japan' },
        ];
      }

      TrackBymetod(index: string, countries: any): string {
        return countries.id;
      }`,
      html: `
      <div *ngFor="let country of countries; let i=index;TrackBy:TrackBymetod">
          <h3>{{country.id}} {{country.name}}</h3>
      </div>
      <button (click)="getmore()">load more</button>`,
    },
    // grouping with ngFor
    grouping: {
      ts: `
       bank: any[] = [
        {name:'raj' , account:[
          {type:'Personal Account'},
          {type:'Real Account'},
          {type:'Nominal Account'},
        ]},
        {name:'rajesh' , account:[
          {type:'Personal Account'},
          {type:'Real Account'},
          {type:'Nominal Account'},
        ]},
      ];`,
      html: `
       <ul *ngFor="let person of bank">
       <li>{{person.name}}

           <ul *ngFor="let AcType of person.account">
               <li>{{AcType.type}}</li>
           </ul>
           
       </li>
       </ul>`,
    }
  },
}
export const D_Attribute = {
  // ---------ng style---
  ngStyle: {
    static: `
    <div [ngStyle]="{color:'white',background:'blue',height:'100px'}">
       <h1>test</h1>
    </div>`,
    dynamic: {
      html: `
      <div *ngFor="let style of ngStyleData">

      <div [ngStyle]="{ background: methood(style.result) }">
      <p>name : {{ style.name }} & result : {{ style.result }}</p>
      </div>

      </div>`,
      ts: `
      ngStyleData: any[] = [
        { name: 'raj', result: 'fail' },
        { name: 'rajsh', result: 'pass' },
        { name: 'raju', result: 'distinction' },
      ];

      methood(result) {
        switch (result) {
          case 'fail':
            return "red";
          case 'pass':
            return "yellow";
          case 'distinction':
            return "green";
        }
      }`,
    },
  },
   // ---------ng class---
  ngClass: {
    type: {
      str: `
      <div [ngClass]=" 'class1 class2' " ></div>
      `,
      arr: `
      <div [ngClass]="['class1' , 'class2']" ></div>
      `,
      objects: `
      <div [ngClass]="{class1:true, class2:false}" ></div>
      `,
    },
    Dynamic: {
      html: `
      <div [ngClass]="method()" ></div>`,
      css: `
      .classOne{
        color: white;
    }
    .classTwo{
       background-color: tomato;
       height: 50px;
    }`,
      ts: `
      method() {
        let miltcss = {
          classOne: true,
          classTwo: true
        }
      return miltcss;
        }`,
    },
  },
}