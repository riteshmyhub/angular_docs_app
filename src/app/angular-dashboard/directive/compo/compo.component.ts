import { Component, OnInit } from '@angular/core';
import { D_Component } from "../code/source-code-directive"
import { from } from 'rxjs';
@Component({
  selector: 'app-compo',
  templateUrl: './compo.component.html',
  styleUrls: ['./compo.component.css']
})
export class CompoComponent implements OnInit {
  D_Component = D_Component;
  constructor() { }
  alertEvent() {
    alert("component directive working");
  }
  ngOnInit(): void {
  }

}
