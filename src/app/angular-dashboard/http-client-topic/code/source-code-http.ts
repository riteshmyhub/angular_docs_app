export const HttpList = [
    { id: 1, point: 'What Is HttpClient', path: 'What-Is-HttpClient' },
    { id: 2, point: 'Get', path: 'Get' },
    { id: 3, point: 'Post', path: 'Post' },
    { id: 4, point: 'Put', path: 'Put' },
    { id: 5, point: 'Delete', path: 'Delete' },
]
