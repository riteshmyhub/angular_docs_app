import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HttpClientTopicRoutingModule } from './http-client-topic-routing.module';
import { WhatIsHttpClientComponent } from './what-is-http-client/what-is-http-client.component';
import { HttpGetComponent } from './curd/http-get/http-get.component';
import { HttpPostComponent } from './curd/http-post/http-post.component';
import { HttpPutComponent } from './curd/http-put/http-put.component';
import { HttpDeleteComponent } from './curd/http-delete/http-delete.component';
import { GetService } from './curd/http-get/service/get.service';
import { HttpClientModule } from '@angular/common/http';
import { SharedModule } from 'src/app/shared/shared.module';
import { PostService } from './curd/http-post/service/post.service';
import { PutService } from './curd/http-put/service/put.service'
import { from } from 'rxjs';
import { DeleteService } from './curd/http-delete/service/delete.service';
@NgModule({
  declarations: [WhatIsHttpClientComponent, HttpGetComponent, HttpPostComponent, HttpPutComponent, HttpDeleteComponent],
  imports: [
    CommonModule,
    HttpClientTopicRoutingModule,
    HttpClientModule,
    SharedModule
  ],
  providers: [GetService, PostService,PutService,DeleteService]
})
export class HttpClientTopicModule { }
