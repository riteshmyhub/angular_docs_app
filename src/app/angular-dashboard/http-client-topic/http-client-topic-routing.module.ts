import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HttpClientTopicComponent } from './http-client-topic.component';
import { WhatIsHttpClientComponent } from './what-is-http-client/what-is-http-client.component';
import { HttpGetComponent } from './curd/http-get/http-get.component';
import { HttpPostComponent } from './curd/http-post/http-post.component';
import { HttpPutComponent } from './curd/http-put/http-put.component';
import { HttpDeleteComponent } from './curd/http-delete/http-delete.component';

const routes: Routes = [
  { path: '', component: HttpClientTopicComponent ,children:[
    {path:'',component:WhatIsHttpClientComponent},
    {path:'What-Is-HttpClient',component:WhatIsHttpClientComponent},
    {path:'Get',component:HttpGetComponent},
    {path:'Post',component:HttpPostComponent},
    {path:'Put',component:HttpPutComponent},
    {path:'Delete',component:HttpDeleteComponent},
  ]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HttpClientTopicRoutingModule { }
