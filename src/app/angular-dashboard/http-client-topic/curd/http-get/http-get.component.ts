import { Component, OnInit } from '@angular/core';
import { GetService } from './service/get.service';

@Component({
  selector: 'app-http-get',
  templateUrl: './http-get.component.html',
  styleUrls: ['./http-get.component.css']
})
export class HttpGetComponent implements OnInit {
  userList;
  constructor(private service: GetService) { }

  ngOnInit(): void {
    this.usersGet();
  }
  usersGet() {
    this.service.get_service().subscribe(xyz => this.userList = xyz)
  }
}
