import { Injectable } from '@angular/core';
import { user } from '../../model/user.model';
import { HttpClient } from '@angular/common/http';
import { Observable, from } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class GetService {
  url = 'http://localhost:3000/users'
  constructor(private http: HttpClient) { }
  get_service(): Observable<user[]> {
    return this.http.get<user[]>(this.url)
  }
}
