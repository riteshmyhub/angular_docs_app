import { Component, OnInit } from '@angular/core';
import { PostService } from './service/post.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { user } from '../model/user.model';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-http-post',
  templateUrl: './http-post.component.html',
  styleUrls: ['./http-post.component.css']
})
export class HttpPostComponent implements OnInit {
  done = false;//success message
  userFrom: FormGroup;
  userList;
  constructor(private FB: FormBuilder, private service: PostService) { }

  ngOnInit(): void {
    // all form input
    this.userFrom = this.FB.group({
      id: ["", [Validators.required]],
      name: ["", [Validators.required]],
      lastname: ["", [Validators.required]],
      email: ["", [Validators.required]],
      language: ["", [Validators.required]],
    })
    // call 
    this.users_get()
  }
  submit() {
    this.done = false;
    let output = this.userFrom.value;
    console.log(output);
    this.create_user(output)
    this.userFrom.reset();
  }
  // post method call from service
  create_user(xyz: user) {
    this.service.post_service(xyz).subscribe(xyz => {
      this.done = true;//success message show
      this.users_get();
    })
  }
  // get method call from service
  users_get() {
    this.service.get_service().subscribe(xyz => this.userList = xyz)
  }
}
