import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { from, Observable } from 'rxjs';
import { user } from '../../model/user.model'
@Injectable({
  providedIn: 'root'
})
export class PostService {
  url = "http://localhost:3000/users"
  constructor(private http: HttpClient) { }

  // post method
  post_service(Dt: user): Observable<user[]> {
    let httpHeaders = new HttpHeaders().set('content-Types', 'application/Json');
    let options = {
      headers: httpHeaders
    };
    return this.http.post<user[]>(this.url, Dt, options)
  }
  // get method
  get_service(): Observable<user[]> {
    return this.http.get<user[]>(this.url)
  }
}
