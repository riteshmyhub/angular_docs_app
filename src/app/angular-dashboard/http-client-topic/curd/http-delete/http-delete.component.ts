import { Component, OnInit } from '@angular/core';
import { DeleteService } from './service/delete.service'
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { user } from '../model/user.model';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-http-delete',
  templateUrl: './http-delete.component.html',
  styleUrls: ['./http-delete.component.css']
})
export class HttpDeleteComponent implements OnInit {
  message;
  // additional ignore 
  done = false;//success message
  userFrom: FormGroup;
  userList;
  updateViaId = null;
  constructor(private FB: FormBuilder, private service: DeleteService) { }
  ngOnInit() {
    // all form input
    this.userFrom = this.FB.group({
      id: ["", [Validators.required]],
      name: ["", [Validators.required]],
      lastname: ["", [Validators.required]],
      email: ["", [Validators.required]],
      language: ["", [Validators.required]],
    })
    // call 
    this.users_get()
  }
  editNow(item: number) {
    this.service.user_by_id(item).subscribe(xyz => {
      this.updateViaId = item;
      this.userFrom.controls["id"].setValue(xyz.id)
      this.userFrom.controls["name"].setValue(xyz.name)
      this.userFrom.controls["lastname"].setValue(xyz.lastname)
      this.userFrom.controls["email"].setValue(xyz.email)
      this.userFrom.controls["language"].setValue(xyz.language)
    })
  }
  submit() {
    this.done = false;
    let output = this.userFrom.value;
    console.log(output);
    this.create_user(output)
    this.userFrom.reset();
  }
  // post method call from service
  create_user(xyz: user) {
    if (this.updateViaId == null) {
      this.service.post_service(xyz).subscribe(xyz => {
        this.done = true;//success message show
        this.message = 'create new user!';
        this.users_get();
      })

    } else {
      xyz.id = this.updateViaId;
      this.service.put_service(xyz).subscribe(abc => {
        this.done = true;
        this.message = 'user updated!';
        this.users_get();
        this.updateViaId = null;
      })
    }
    setTimeout(() => {
      this.done = false;
    }, 3000);
  }
  // get method call from service
  users_get() {
    this.service.get_service().subscribe(xyz => this.userList = xyz)
  }
  // delete method call from service
  delete_user(userId) {
    this.service.delete_service(userId).subscribe(xyz => {
      this.users_get()
    })
  }
}
