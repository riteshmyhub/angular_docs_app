import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http"
import { Observable, from } from 'rxjs';
import { user } from '../../model/user.model'
@Injectable({
  providedIn: 'root'
})
export class DeleteService {
  url = 'http://localhost:3000/users';
  constructor(private http: HttpClient) { }
  // use id
  user_by_id(userid: number) {
    return this.http.get<user>(this.url + '/' + userid)
    // TODO only pass user for get id not array
  }
  // _____________post method______________
  post_service(Dt: user): Observable<user[]> {
    let httpHeaders = new HttpHeaders().set('content-Types', 'application/Json');
    let options = {
      headers: httpHeaders
    };
    return this.http.post<user[]>(this.url, Dt, options)
  }
  // _______________put method______________
  put_service(Dt: user): Observable<number> {
    let httpHeaders = new HttpHeaders().set('content-Types', 'application/Json');
    let options = {
      headers: httpHeaders
    };
    return this.http.put<number>(this.url + '/' + Dt.id, Dt, options)
  }
  // _______________Delete method______________
  delete_service(Dt: user): Observable<number> {
    let httpHeaders = new HttpHeaders().set('content-Types', 'application/Json');
    let options = {
      headers: httpHeaders
    };
    return this.http.delete<number>(this.url + '/' + Dt, options)
  }
  // ____________get method______________________
  get_service(): Observable<user[]> {
    return this.http.get<user[]>(this.url)
  }
}