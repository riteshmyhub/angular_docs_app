export const ngList = [
    { id: 1, point: 'Template reference variables', path: 'Template-reference-variables' },
    { id: 1, point: 'ng template', path: 'Ng-Template' },
    { id: 1, point: 'ng container', path: 'Ng-Container' },
]
export const desugars = {
    ngTemp:{
        Ngif:{
            syntax:`
            <ng-template [ngIf]="true">
              <p>this is text from ng template</p>
            </ng-template>`,
            referenceVar:`
            <div *ngIf="true; else ElseBlock">
                <p>if part</p>
             </div>
            <ng-template #ElseBlock>
                <p>else part</p>
             </ng-template>`,
        },
        Ngfor:{
            syntax:`
        <ng-template ngFor let-variableName [ngForOf]="collectionName" let-i="index">
            {{ i + 1 }} : {{ user.name }} {{ user.sub }} <br />
        </ng-template>`
        },
        Ngswitch:{
            syntax:`
            <div [ngSwitch]="'two'">
                   <!-- case 1 -->
                <ng-template [ngSwitchCase]="'one'">
                    <h1>you selected one</h1>
                </ng-template>
                     <!-- case 2 -->
               <ng-template [ngSwitchCase]="'two'">
                    <h1>you selected two</h1>
                </ng-template>
                     <!--Default-->
                <ng-template ngSwitchDefault>
                    <h1>this is default</h1>
                 </ng-template>
            </div>`
        },
    },
    ngContainer:{
       twoDirective:`
        <table class="table">
           <ng-container *ngFor="let item of data">
             <tr *ngIf="item.id > 1">
               <td>{{ item.id }}</td>
               <td>{{ item.name }}</td>
             </tr>
           </ng-container>
        </table>`,
       forError:`
       <input type="text" />
         <ng-container>
           invalid input
         </ng-container>`,
       DynamicAddContent:{
           ts:`
           Mydata={
            user:"ritesh",
           }`,
           html:`
           <ng-container *ngTemplateOutlet="xyz ; context:Mydata"> 
             </ng-container>
            <ng-template #xyz let-name = "user"> 
                hello {{name}}
            </ng-template>`,
       },
    },
    TempRefVa:{
      ts:`
      clickTemp(param) {
       console.log(param);
     }`,
      html:`
       <input type="text" value="alex" #name />
       <h3>{{ name.value }}</h3>
       <button (click)="clickTemp(name.value)">click me</button>

       <a #text>hello , angular </a>
       <h4>this is innerText : {{ text.innerText }}</h4>`,
    }
}