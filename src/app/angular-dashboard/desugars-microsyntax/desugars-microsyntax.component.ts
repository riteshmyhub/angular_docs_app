import { Component, OnInit } from '@angular/core';
import { ngList } from './code/source-code-ng';

@Component({
  selector: 'app-desugars-microsyntax',
  templateUrl: './desugars-microsyntax.component.html',
  styleUrls: ['./desugars-microsyntax.component.css']
})
export class DesugarsMicrosyntaxComponent implements OnInit {
  ngList = ngList;
  constructor() { }

  ngOnInit(): void {
  }

}
