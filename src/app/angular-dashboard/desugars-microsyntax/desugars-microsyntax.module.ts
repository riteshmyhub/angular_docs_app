import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DesugarsMicrosyntaxRoutingModule } from './desugars-microsyntax-routing.module';
import { NgContainerComponent } from './types/ng-container/ng-container.component';
import { TempRefVarComponent } from './types/temp-ref-var/temp-ref-var.component';


@NgModule({
  declarations: [NgContainerComponent, TempRefVarComponent],
  imports: [
    CommonModule,
    DesugarsMicrosyntaxRoutingModule
  ]
})
export class DesugarsMicrosyntaxModule { }
