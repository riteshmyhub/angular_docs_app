import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DesugarsMicrosyntaxComponent } from './desugars-microsyntax.component';
import { NgTemplateComponent } from './types/ng-template/ng-template.component';
import { NgContainerComponent } from './types/ng-container/ng-container.component';
import { TempRefVarComponent } from './types/temp-ref-var/temp-ref-var.component';

const routes: Routes = [
  {path:'',component:DesugarsMicrosyntaxComponent,children:[
    {path:'',component:TempRefVarComponent},
    {path:'Template-reference-variables',component:TempRefVarComponent},
    {path:'Ng-Template',component:NgTemplateComponent},
    {path:'Ng-Container',component:NgContainerComponent},
  ]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DesugarsMicrosyntaxRoutingModule { }
