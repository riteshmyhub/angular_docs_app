import { Component, OnInit } from '@angular/core';
import { desugars } from '../../code/source-code-ng';

@Component({
  selector: 'app-ng-container',
  templateUrl: './ng-container.component.html',
  styleUrls: ['./ng-container.component.css']
})
export class NgContainerComponent implements OnInit {
  desugars = desugars;
  constructor() { }

  ngOnInit(): void {
  }
  data = [
    { id: 1, name: 'raj' },
    { id: 2, name: 'rajesh' },
    { id: 3, name: 'raju' },
    { id: 4, name: 'rajender' },
    { id: 5, name: 'raja' },
    { id: 6, name: 'rajvaar' },
  ]
  // dynamic add content in ng-container
  Mydata = {
    user: "ritesh",
  }
}
