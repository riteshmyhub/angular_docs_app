import { Component, OnInit } from '@angular/core';
import { ngList, desugars } from '../../code/source-code-ng';
@Component({
  selector: 'app-ng-template',
  templateUrl: './ng-template.component.html',
  styleUrls: ['./ng-template.component.css']
})
export class NgTemplateComponent implements OnInit {
  ngList = ngList;
  desugars=desugars;
  constructor() { }

  ngOnInit(): void {
  }

}
