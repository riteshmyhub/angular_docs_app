import { Component, OnInit } from '@angular/core';
import { desugars } from '../../code/source-code-ng';

@Component({
  selector: 'app-temp-ref-var',
  templateUrl: './temp-ref-var.component.html',
  styleUrls: ['./temp-ref-var.component.css']
})
export class TempRefVarComponent implements OnInit {
  desugars=desugars;
  constructor() { }

  ngOnInit(): void {
 
  }
  clickTemp(param) {
    console.log(param);
  }
}
