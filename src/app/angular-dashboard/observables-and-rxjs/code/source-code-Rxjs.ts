export const RxjsList = [
  { id: 1, point: 'what is observables', path: 'what-is-observables' },
  { id: 2, point: 'Subscribe Web API', path: 'observables_Subscribe' },
  { id: 3, point: 'Async Pipe', path: 'Async-Pipe' },
  { id: 4, point: 'Async With Ngif', path: 'Async-With-Ngif' },
  { id: 5, point: 'Promise', path: 'Promise' },
]
export const RxjsCode = {
  Subscribe: {
    service: `
        import { Injectable } from '@angular/core';
        import { HttpClient } from "@angular/common/http";
        import { Observable, from } from 'rxjs';

        @Injectable({
          providedIn: 'root'
        })
        export class DemoService {
          url = 'https://jsonplaceholder.typicode.com/users';
          constructor(private http: HttpClient) { }
          getData(): Observable<any[]> {
            return this.http.get<any[]>(this.url)
          }
        }`,
    ts: `
        listing = [];
        constructor(private service: DemoService) { }

        ngOnInit() {
          this.getdata();
        }
        getdata() {
          this.service.getData().subscribe(xyz => this.listing = xyz);
         }`,
    html: `
       <table>
         <tbody>
            <tr *ngFor="let item of listing">
                <td>{{item.id}}</td>
                <td>{{item.name}}</td>
                <td>{{item.username}}</td>
                <td>{{item.email}}</td>
            </tr>
         </tbody>
      </table>`,
    asyncPipe: {
      model: `
        export interface myType {
          id: number,
          name: string,
          username: string,
          email: any,
          website: any,
        }`,
      ts: `
           list: Observable<myType[]>;
           constructor(private service: DemoService) { }
      
           ngOnInit(): void {
             this.method();
           }
           method() {
             this.list = this.service.getData()
           }`,
      html: `
         <table>
           <tbody>
              <tr *ngFor="let item of list | async">
                  <td>{{item.id}}</td>
                  <td>{{item.name}}</td>
                  <td>{{item.username}}</td>
                  <td>{{item.email}}</td>
              </tr>
           </tbody>
        </table>`,
    },
  },
  asyncPipeIf: {
    service: `
    import { Injectable } from '@angular/core';
    import { HttpClient } from "@angular/common/http";
    import { Observable, from } from 'rxjs';
    import { myType } from '../../obs-async-pipe/model/myType';

    @Injectable({
      providedIn: 'root'
    })
    export class AsyncIfService {
      url = 'https://jsonplaceholder.typicode.com/users';

      constructor(private http: HttpClient) { }
      getData(id:number): Observable<myType[]> {
        return this.http.get<myType[]>(this.url+'/'+id)
      }
    }`,
    ts: `
    listing: Observable<myType[]>;
    constructor(private service: AsyncIfService) { }
  
    ngOnInit() {
      this.method()
    }
    method() {
      this.listing = this.service.getData(1)
    }`,
    html: `
    <div *ngIf="listing | async as show; else: loading">
          <h6>id : {{ show.id }}</h6>
          <h6>name : {{ show.name }}</h6>
          <h6>username : {{ show.username }}</h6>
          <h6>email : {{ show.email }}</h6>
          <h6>website : {{ show.website }}</h6>
    </div>
    <ng-template #loading>
        <div class="centerXY">
            <div class="spinner-border text-danger" role="status">
                <span class="sr-only">Loading...</span>
            </div>
        </div>
    </ng-template>`,
  },
  promise: {
    syntax: {
      one: `
      ngOnInit() { 

        let xyz = new Promise((resolve, reject)=>{
          resolve('Promise in resolve')
             //(or - any one)
          reject('i have not Both item')})
        }
  
        // use xyz variable 
        xyz.then(res => {
          console.log('(then Code)', res);
        }).catch(res => {
          console.log('(catch Code)', res);
        })
      }`,
    },
    example: {
      html: `
      <strong>{{message}}</strong>
      <button class="btn btn-primary" (click)="itemCheck()">check item</button>`,
      ts: `
      message: any;
      constructor() { }

      // item 1 
      haveWater() {
        return false;
      }
      // item 2 
      haveFood() {
        return true;
      }

       itemCheck() {
         let xyz = new Promise((resolve, reject) => {

           // for item 1
           if (this.haveWater()) {
             return setTimeout(() => {
               resolve('yes, i have water')
             }, 4000);
           }

           // for item 2
           else if (this.haveFood()) {
             return setTimeout(() => {
               resolve('yes, i have Food')
             }, 4000);
           }

           // for not Both item
           else {
             reject('i have not Both item')
           }

         })
         
         // use xyz variable
         xyz.then(res => {
           console.log('(then Code)', res);
           this.message = res;
         }).catch(res => {
           console.log('(catch Code)', res);
           this.message = res;
         })
       }`
    }
  }
}