import { Component, OnInit } from '@angular/core';
import { Observable, from } from 'rxjs';
import { DemoService } from '../observ-subscribe/service/demo.service';
import { myType } from './model/myType';
import { RxjsCode } from '../code/source-code-Rxjs';
@Component({
  selector: 'app-obs-async-pipe',
  templateUrl: './obs-async-pipe.component.html',
  styleUrls: ['./obs-async-pipe.component.css']
})
export class ObsAsyncPipeComponent implements OnInit {
  RxjsCode = RxjsCode;

  list: Observable<myType[]>;
  constructor(private service: DemoService) { }

  ngOnInit(): void {
    this.method();
  }
  method() {
    this.list = this.service.getData()
  }
}
