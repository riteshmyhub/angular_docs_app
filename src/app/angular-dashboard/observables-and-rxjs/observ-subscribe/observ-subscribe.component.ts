import { Component, OnInit } from '@angular/core';
import { DemoService } from './service/demo.service';
import { RxjsCode } from '../code/source-code-Rxjs';

@Component({
  selector: 'app-observ-subscribe',
  templateUrl: './observ-subscribe.component.html',
  styleUrls: ['./observ-subscribe.component.css']
})
export class ObservSubscribeComponent implements OnInit {
  RxjsCode = RxjsCode;

  listing = [];
  constructor(private service: DemoService) { }

  ngOnInit() {
    this.getdata();
  }
  getdata() {
    this.service.getData().subscribe(xyz => this.listing = xyz);
  }
}
