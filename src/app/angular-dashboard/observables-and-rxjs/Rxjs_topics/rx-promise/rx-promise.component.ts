import { Component, OnInit } from '@angular/core';
import { RxjsCode } from '../../code/source-code-Rxjs';
@Component({
  selector: 'app-rx-promise',
  templateUrl: './rx-promise.component.html',
  styleUrls: ['./rx-promise.component.css']
})
export class RxPromiseComponent implements OnInit {
  RxjsCode = RxjsCode;
  message: any;
  constructor() { }

  // item 1 
  haveWater() {
    return false;
  }
  // item 2 
  haveFood() {
    return true;
  }
  itemCheck() {
    let xyz = new Promise((resolve, reject) => {
      // for item 1
      if (this.haveWater()) {
        return setTimeout(() => {
          resolve('yes, i have water')
        }, 4000);
      }
      // for item 2
      else if (this.haveFood()) {
        return setTimeout(() => {
          resolve('yes, i have Food')
        }, 4000);
      }
      // for not Both item
      else {
        reject('i have not Both item')
      }
    })
    xyz.then(res => {
      console.log('(then Code)', res);
      this.message = res;
    }).catch(res => {
      console.log('(catch Code)', res);
      this.message = res;
    })
  }
  ngOnInit() { }

}
