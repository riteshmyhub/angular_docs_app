import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from "@angular/common/http";
import { ObservablesAndRxjsRoutingModule } from './observables-and-rxjs-routing.module';
import { WhatIsObservablesComponent } from './what-is-observables/what-is-observables.component';
import { ObservSubscribeComponent } from './observ-subscribe/observ-subscribe.component';
import { DemoService } from './observ-subscribe/service/demo.service'
import { from } from 'rxjs';
import { ObsAsyncPipeComponent } from './obs-async-pipe/obs-async-pipe.component';
import { AsyncWithNgifComponent } from './async-with-ngif/async-with-ngif.component';
import { AsyncIfService } from './async-with-ngif/service/async-if.service';
import { RxPromiseComponent } from './Rxjs_topics/rx-promise/rx-promise.component';

@NgModule({
  declarations: [WhatIsObservablesComponent, ObservSubscribeComponent, ObsAsyncPipeComponent, AsyncWithNgifComponent, RxPromiseComponent],
  imports: [
    CommonModule,
    ObservablesAndRxjsRoutingModule,
    HttpClientModule
  ],
  providers: [DemoService, AsyncIfService]

})
export class ObservablesAndRxjsModule { }
