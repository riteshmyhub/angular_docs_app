import { Component, OnInit } from '@angular/core';
import { RxjsList } from './code/source-code-Rxjs';
@Component({
  selector: 'app-observables-and-rxjs',
  templateUrl: './observables-and-rxjs.component.html',
  styleUrls: ['./observables-and-rxjs.component.css']
})
export class ObservablesAndRxjsComponent implements OnInit {
  RxjsList = RxjsList;
  constructor() { }

  ngOnInit(): void {
  }

}
