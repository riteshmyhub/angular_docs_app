import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ObservablesAndRxjsComponent } from './observables-and-rxjs.component';
import { WhatIsObservablesComponent } from './what-is-observables/what-is-observables.component';
import { ObservSubscribeComponent } from './observ-subscribe/observ-subscribe.component';
import { ObsAsyncPipeComponent } from './obs-async-pipe/obs-async-pipe.component';
import { AsyncWithNgifComponent } from './async-with-ngif/async-with-ngif.component';
import { RxPromiseComponent } from './Rxjs_topics/rx-promise/rx-promise.component';

const routes: Routes = [
  {path:'',component:ObservablesAndRxjsComponent,children:[
    {path:'',component:WhatIsObservablesComponent},
    {path:'what-is-observables',component:WhatIsObservablesComponent},
    {path:'observables_Subscribe',component:ObservSubscribeComponent},
    {path:'Async-Pipe',component:ObsAsyncPipeComponent},
    {path:'Async-With-Ngif',component:AsyncWithNgifComponent},
    {path:'Promise' , component:RxPromiseComponent}
  ]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ObservablesAndRxjsRoutingModule { }
