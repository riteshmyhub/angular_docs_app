import { Component, OnInit } from '@angular/core';
import { AsyncIfService } from './service/async-if.service';
import { myType } from '../obs-async-pipe/model/myType';
import { Observable } from 'rxjs';
import { RxjsCode } from '../code/source-code-Rxjs';

@Component({
  selector: 'app-async-with-ngif',
  templateUrl: './async-with-ngif.component.html',
  styleUrls: ['./async-with-ngif.component.css']
})
export class AsyncWithNgifComponent implements OnInit {
  RxjsCode=RxjsCode;
  
  listing: Observable<myType[]>;
  constructor(private service: AsyncIfService) { }

  ngOnInit() {
    this.method()
  }
  method() {
    this.listing = this.service.getData(1)
  }
}
