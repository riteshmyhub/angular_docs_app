import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Observable, from } from 'rxjs';
import { myType } from '../../obs-async-pipe/model/myType';

@Injectable({
  providedIn: 'root'
})
export class AsyncIfService {
  url = 'https://jsonplaceholder.typicode.com/users';

  constructor(private http: HttpClient) { }
  getData(id:number): Observable<myType[]> {
    return this.http.get<myType[]>(this.url+'/'+id)
  }
}
