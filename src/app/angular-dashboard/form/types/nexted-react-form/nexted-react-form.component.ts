import { Component, OnInit } from '@angular/core';
import { formCode } from '../../code/source-code-Form';
import { FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-nexted-react-form',
  templateUrl: './nexted-react-form.component.html',
  styleUrls: ['./nexted-react-form.component.css']
})
export class NextedReactFormComponent implements OnInit {
  formCode = formCode;
  myform;
  constructor(private Fb: FormBuilder) { }
  output;
  formSubmit() {
    this.output = (this.myform.value);
  }
  ngOnInit() {
    this.myform = this.Fb.group({
      name: [''],

      address: this.Fb.group({
        Country: [""],
        State: [""],
        City: [""]
      })

    })
  }

}
