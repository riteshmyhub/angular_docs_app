import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from "@angular/forms"
import { formCode } from 'src/app/angular-dashboard/form/code/source-code-Form';

@Component({
  selector: 'app-simple-reactive',
  templateUrl: './simple-reactive.component.html',
  styleUrls: ['./simple-reactive.component.css']
})
export class SimpleReactiveComponent implements OnInit {
  formCode =formCode;

  myform: FormGroup;
  ouptut;
  constructor() { }
  rectiveForm() {
    this.ouptut = (this.myform.value);
    console.log(this.ouptut);
  }
  ngOnInit() {
    this.myform = new FormGroup({
      name: new FormControl(''),
      pass: new FormControl(''),
    });
  }

}
