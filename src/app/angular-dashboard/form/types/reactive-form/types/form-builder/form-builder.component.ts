import { Component, OnInit } from '@angular/core';
import { formCode } from 'src/app/angular-dashboard/form/code/source-code-Form';
import { FormBuilder } from '@angular/forms';
@Component({
  selector: 'app-form-builder',
  templateUrl: './form-builder.component.html',
  styleUrls: ['./form-builder.component.css']
})
export class FormBuilderComponent implements OnInit {
  formCode = formCode;
  myForm;
  constructor(private fB: FormBuilder) { }
  output;
  FormBuil() {
    this.output = this.myForm.value;
  }
  ngOnInit() {
    this.myForm = this.fB.group({
      username: [""],
      password: [""],
    })
  }

}
