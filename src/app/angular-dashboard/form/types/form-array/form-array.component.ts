import { Component, OnInit } from '@angular/core';
import { formCode } from '../../code/source-code-Form';
import { FormArray, FormBuilder, FormControl, Validators } from "@angular/forms";
@Component({
  selector: 'app-form-array',
  templateUrl: './form-array.component.html',
  styleUrls: ['./form-array.component.css']
})
export class FormArrayComponent implements OnInit {
  formCode=formCode;
  myForm;

  constructor(private FB: FormBuilder) { }
  output = "";
  fromSubmit() {
    this.output = JSON.stringify(this.myForm.value);
    console.log();
  }

  ngOnInit() {
    this.myForm = this.FB.group({
      name: [""],
      Friends: new FormArray([new FormControl("")]),
    });
  }

  get Friends() {
    return this.myForm.get("Friends");
  }

  addInput() {
    const newRow = new FormControl(null);
    (<FormArray>this.myForm.get("Friends")).push(newRow);
  }

  deleteInput(index) {
    const newRow = <FormArray>this.myForm.controls["Friends"];
    if (index > 0) {
      newRow.removeAt(index);
    } else {
      alert("One Friend must be Enter");
    }
  }

}
