import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormComponent } from './form.component';
import { WhatIsFormComponent } from './what-is-form/what-is-form.component';
import { ReactiveFormComponent } from './types/reactive-form/reactive-form.component';
import { TemplateFormComponent } from './types/template-form/template-form.component';
import {FormArrayComponent} from './types/form-array/form-array.component';
import { SimpleReactiveComponent } from './types/reactive-form/types/simple-reactive/simple-reactive.component';
import { FormBuilderComponent } from './types/reactive-form/types/form-builder/form-builder.component';
import { NextedReactFormComponent } from './types/nexted-react-form/nexted-react-form.component';
const routes: Routes = [
  {path:'',component:FormComponent, children:[
    {path:'',component:WhatIsFormComponent},
    {path:'what-is-form',component:WhatIsFormComponent},
    {path:'template-form',component:TemplateFormComponent},
    {path:'reactive-Form',component:ReactiveFormComponent},
    {path:'nexted-reactive',component:NextedReactFormComponent},
    {path:'',children:[
      {path:'form-array',component:FormArrayComponent},
      {path:'SimpleReactive',component:SimpleReactiveComponent},
      {path:'FormBuilder',component:FormBuilderComponent}
    ]}
  ]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FormRoutingModule { }
