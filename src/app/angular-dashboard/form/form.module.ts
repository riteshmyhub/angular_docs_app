import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormRoutingModule } from './form-routing.module';
import { WhatIsFormComponent } from './what-is-form/what-is-form.component';
import { TemplateFormComponent } from './types/template-form/template-form.component';
import { ReactiveFormComponent } from './types/reactive-form/reactive-form.component';
import { FormArrayComponent } from './types/form-array/form-array.component';
import { SimpleReactiveComponent } from './types/reactive-form/types/simple-reactive/simple-reactive.component';
import { FormBuilderComponent } from './types/reactive-form/types/form-builder/form-builder.component';
import { NextedReactFormComponent } from './types/nexted-react-form/nexted-react-form.component';
// shard mode
import { SharedModule } from '../../shared/shared.module'
import { from } from 'rxjs';


@NgModule({
  declarations: [WhatIsFormComponent, TemplateFormComponent, ReactiveFormComponent, FormArrayComponent, SimpleReactiveComponent, FormBuilderComponent, NextedReactFormComponent],
  imports: [
    CommonModule,
    FormRoutingModule,
    SharedModule
  ]
})
export class FormModule { }
