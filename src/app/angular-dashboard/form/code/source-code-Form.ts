export const FormList = [
    { id: 1, point: 'what is form', path: 'what-is-form' },
    { id: 2, point: 'template form', path: 'template-form' },
    { id: 3, point: 'reactive Form', path: 'reactive-Form' },
    { id: 4, point: 'nexted reactive Form', path: 'nexted-reactive' },
    { id: 5, point: 'form array', path: 'form-array' },
]
export const formCode = {
    template: {
        syntax: `
     <form #myForm="ngForm" (ngSubmit)="method(myForm)">

        <input type="text" name="name" ngModel />

        <input type="text" name="email" ngModel />

        <button type="submit" id="reg">Submit Now</button>
   </form>`,
        appMode: `
    import { FormsModule } from '@angular/forms';`,
        html: `
    <form #myForm="ngForm" (ngSubmit)="formSub(myForm)">

        <input type="text" name="user" ngModel>
        <input type="password" name="pass" ngModel>

        <button type="submit">Submit</button>
    </form>`,
        ts: `
    formSub(myForm) {
        let a = myForm.value;
        let b = myForm.control;
        console.log(a);
      }`,
    },
    reactive:{
        simple:{
            appMode:`
            import { ReactiveFormsModule } from '@angular/forms';`,
            ts:`
            import { FormControl, FormGroup } from "@angular/forms"

            export class ReactivFormComponent implements OnInit {
              myform: FormGroup;
              ouptut;
              constructor() { }
              rectiveForm() {
                this.ouptut = (this.myform.value);
                console.log(this.ouptut);
              }
              ngOnInit() {
                this.myform = new FormGroup({
                  name: new FormControl(''),
                  pass: new FormControl(''),
                });
              }

            }`,
            html:`
            <form [formGroup]="myform" (ngSubmit)="rectiveForm()">
               <h4>enter your name</h4>
               <input type="text" formControlName="name" />
    
                <h4>enter your password</h4>
                <input type="password" formControlName="pass" />
                <br />
            <button type="submit">submit here</button>
             </form>
            {{ ouptut | json}}`,
        },
        formBuilder:{
           appMode:`
           import { ReactiveFormsModule } from '@angular/forms';`,
           ts:`
           import { FormBuilder} from '@angular/forms';
           
           export class FormBuilderComponent implements OnInit {
               myForm;
               constructor(private fB: FormBuilder) { }
               output;
               FormBuil() {
                 this.output = this.myForm.value;
               }
               ngOnInit(){
                 this.myForm = this.fB.group({
                  username:[""], 
                  password:[""], 
                 })
               }
             }`,
             html:`
        <form [formGroup]="myForm" (ngSubmit)="FormBuil()">
           <span>name:</span><br>
           <input type="text" formControlName="username"><br>
           <span>password:</span><br>
           <input type="password" formControlName="password"><br><br>
           <button type="submit">submit here</button>
       </form>
      {{output | json}}`,
      },
      nextedRecatForm:{
          ts:`        
           import { FormBuilder } from '@angular/forms';

           myform;
           constructor(private Fb: FormBuilder) {}
           output;
           formSubmit() {
             this.output = (this.myform.value);
           }
           ngOnInit() {
             this.myform = this.Fb.group({
               name:[''],

               address:this.Fb.group({
                 Country:[""],
                 State:[""],
                 City:[""]
               })

             })
           }`,
          html:`
          <form [formGroup]="myform" (ngSubmit)="formSubmit()">
            <input type="text" formControlName="name">
            
            <div formGroupName="address" style="padding-left: 20px;">
               <input type="text" formControlName="Country"><br>
               <input type="text" formControlName="State"><br>
               <input type="text" formControlName="City"><br><br>
            </div>

            <button type="submit">submit now</button>
         </form>
            {{output | json}}`,
      },
    },
    formArray:{
        ts:`
        import { FormArray, FormBuilder, FormControl, Validators } from "@angular/forms";

        myForm;

          constructor(private FB: FormBuilder) { }
          output = "";
          fromSubmit() {
            this.output = JSON.stringify(this.myForm.value);
            console.log();
          }

          ngOnInit() {
            this.myForm = this.FB.group({
              name: [""],
              password: [""],
              Friends: new FormArray([new FormControl("")]),
            });
          }

          get Friends() {
            return this.myForm.get("Friends");
          }

          addInput() {
            const newRow = new FormControl(null);
            (<FormArray>this.myForm.get("Friends")).push(newRow);
          }

          deleteInput(index) {
            const newRow = <FormArray>this.myForm.controls["Friends"];
            if (index > 0) {
              newRow.removeAt(index);
            } else {
              alert("One Friend must be Enter");
            }
          }`,
          html:`
          <form [formGroup]="myForm" (ngSubmit)="fromSubmit()">

                  <span>Name</span>
                  <input type="text" formControlName="name" />

                  <span>password</span><br />
                  <input type="password" formControlName="password" />

                  <span>enter your friends</span>

            <div formArrayName="Friends">
               <ng-container *ngFor="let rowAll of myForm.get('Friends').controls;index as i">
                  <input type="text" formControlName="{{ i }}"/>
                  <button type="button" (click)="deleteInput(i)">
                   Delete
                  </button>
               </ng-container>
            <button type="button" (click)="addInput()"> + Friends</button>
          </div>
            <button type="submit">submit</button>
      </form>
       <span>{{ output }}</span>`,
    }
}
