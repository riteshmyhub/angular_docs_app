import { Component, OnInit } from '@angular/core';
import { LifeCycleList } from './code/source-code-LifeCycle'
@Component({
  selector: 'app-life-cycle-and-hooks',
  templateUrl: './life-cycle-and-hooks.component.html',
  styleUrls: ['./life-cycle-and-hooks.component.css']
})
export class LifeCycleAndHooksComponent implements OnInit {
  LifeCycleList = LifeCycleList;
  constructor() { }

  ngOnInit(): void {
  }

}
