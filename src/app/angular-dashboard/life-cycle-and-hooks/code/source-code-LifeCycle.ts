export const LifeCycleList = [
    { id: 1, point: 'what is LifeCycle', path: 'what-is-LifeCycle' },
    { id: 1, point: 'Constructor', path: 'Constructor' },
    { id: 1, point: 'Life Cycle Stages', path: 'LifeCycle-Stages' },
]

export const LifeCycleCode = {
    stages: {
        one: {
            child: {
                html: `
                <h5>data from parent : {{ data }}</h5>
                <h5>current value : {{ current_data }}</h5>
                <h5>previous value : {{ previous_data }}</h5>`,
                ts: `
                @Input() data: string;

                current_data = null;
                previous_data = null;
                ngOnChanges(changes) {
                    console.log('ngOnChanges is called');
                    this.current_data = changes.data.currentValue;
                    this.previous_data = changes.data.previousValue;
                }`,
            },
            parent: {
                html: `
                <input type="text" #name (keyup)="(0)" />
                
                <app-life-child [data]="name.value"></app-life-child>`,
                ts: ``,
            },
        },
        two: {
            ngOnInit: {
                ts: `
                ngOnInit() {
                    console.log('ngOnInit in called');
                  }`,
            }
        },
        three: {
            ngDoCheck: {
                ts: `
                myObj = ['one', 'two', 'three'];
                push_thing() {
                  this.myObj.push('four');
                }
                ngDoCheck(): void {
                  console.log('ngDoCheck is called');
                }`,
                html: `
                <div *ngFor="let item of myObj">
                  <h4>{{ item }}</h4>
                </div>
                <button (click)="push_thing()">store data</button>`,
            },
        },
        four: {
            ngAfterContentInit: {
                ts: `
                ngAfterContentInit() {
                    console.log('ngAfterContentInit() is called');
                }`,
            }
        },
        five: {
            ngAfterContentChecked: {
                ts: `
                ngAfterContentChecked(): void {
                    alert('ngAfterContentChecked is called ! you changing content');
                  }`,
                html: `
                  <div *ngFor="let item of myObj">
                  <h4>{{ item }}</h4>
               </div>
                <button (click)="push_thing()">store data</button>`,
            }
        },
        six: {
            ngAfterViewInit: {
                ts: `
                ngAfterViewInit(): void {
                    console.log('ngAfterViewInit has called ');
                  }`,
            }
        },
        seven: {
            ngAfterViewChecked: {
                ts: `
                ngAfterViewChecked(): void {
                    console.log("ngAfterViewChecked is called");
                  }`,
            }
        },
        eight: {
            ngOnDestroy: {
                ts: `
                ngOnDestroy(): void {
                    console.log('ngOnDestroy is called -> current component is Destroy');
                }`,
            }
        }
    },

}