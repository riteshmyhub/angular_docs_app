import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-life-stages',
  templateUrl: './life-stages.component.html',
  styleUrls: ['./life-stages.component.css']
})
export class LifeStagesComponent implements OnInit {
  Stages = [
    { name: 'ngOnChanges', link: '../ngOnChanges' },
    { name: 'ngOnInit', link: '../ngOnInit' },
    { name: 'ngDoCheck', link: '../ngDoCheck' },
    { name: 'ngAfterContentInit', link: '../ngAfterContentInit' },
    { name: 'ngAfterContentChecked', link: '../ngAfterContentChecked' },
    { name: 'ngAfterViewInit', link: '../ngAfterViewInit' },
    { name: 'ngAfterViewChecked', link: '../ngAfterViewChecked' },
    { name: 'ngOnDestroy', link: '../ngOnDestroy' },
  ]

  constructor() { }

  ngOnInit(): void {
  }

}
