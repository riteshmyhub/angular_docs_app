import { Component, OnInit } from '@angular/core';
import { LifeCycleCode } from 'src/app/angular-dashboard/life-cycle-and-hooks/code/source-code-LifeCycle';

@Component({
  selector: 'app-hook-two',
  template: `
    <h1 style="text-transform: unset;">
    2 . ngOnInit()
    </h1>
    <hr>
    <p>ngOnInit() method is default add in commponent</p>
    <p>ngOnInit() method is called only for one time whan component is is initialise or load</p>
    <p>this ngOnInit() method use for data initialise in component</p>
    <p>if we have parant component in child component in that condtion first call ngOnInit() of parant component Than ngOnInit() of child component</p>
    <hr>
    <pre data-doc="1 name.component.html">
      <code>
      {{LifeCycleCode.stages.two.ngOnInit.ts}}
      </code>
    </pre>
  `,
  styles: [
  ]
})
export class HookTwoComponent implements OnInit {
  LifeCycleCode = LifeCycleCode;
  constructor() { }
  ngOnInit() {
    console.log('ngOnInit in called');
  }

}
