import { Component, OnInit } from '@angular/core';
import { LifeCycleCode } from 'src/app/angular-dashboard/life-cycle-and-hooks/code/source-code-LifeCycle';

@Component({
  selector: 'app-hook-one',
  templateUrl: './hook-one.component.html',
  styleUrls: ['./hook-one.component.css']
})
export class HookOneComponent implements OnInit {
  LifeCycleCode=LifeCycleCode;
  constructor() { }

  ngOnInit(): void {
  }

}
