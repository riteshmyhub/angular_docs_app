import { Component, OnInit } from '@angular/core';
import { LifeCycleCode } from 'src/app/angular-dashboard/life-cycle-and-hooks/code/source-code-LifeCycle';

@Component({
  selector: 'app-hookfive',
  template: `
  <h1 style="text-transform: unset;">
  5 . ngAfterContentChecked()
  </h1>
  <hr>
  <p>this ngAfterContentChecked() method this check changes in content</p>
  <p>this method is dedect continuous check in content</p>
  <hr>
  <pre data-doc="1 name.component.ts">
      <code>
      {{LifeCycleCode.stages.five.ngAfterContentChecked.ts}}
      </code>
    </pre>
    <pre data-doc="2 name.component.html">
      <code>
      {{LifeCycleCode.stages.five.ngAfterContentChecked.html}}
      </code>
    </pre>
  <hr>
  <div class="output">
  <div class="p-4">
    <div *ngFor="let item of myObj">
       <h4>{{ item }}</h4>
    </div>
     <button class="btn btn-primary btn-sm" (click)="push_thing()">store data</button>
  </div>
  </div>
  `,
  styles: [
  ]
})
export class HookfiveComponent implements OnInit {
  LifeCycleCode=LifeCycleCode;
  constructor() { }

  ngOnInit(): void {
  }
  myObj = ['one', 'two', 'three'];
  push_thing() {
    this.myObj.push('four');
  }
  ngAfterContentChecked(): void {
    alert('ngAfterContentChecked is called ! you changing content');
  }
}
