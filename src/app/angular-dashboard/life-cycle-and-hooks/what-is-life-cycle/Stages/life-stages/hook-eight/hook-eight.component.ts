import { Component, OnInit } from '@angular/core';
import { LifeCycleCode } from 'src/app/angular-dashboard/life-cycle-and-hooks/code/source-code-LifeCycle';

@Component({
  selector: 'app-hook-eight',
  template: `
  <h1 style="text-transform: unset;">
  8 . ngOnDestroy()
  </h1>
  <hr>
  <p>when current component is Destroy in that condition called ngOnDestroy()</p>
  <p>component is removed from dom than fire ngOnDestroy() method</p>
  <pre data-doc="1 name.component.html">
  <code>
  {{LifeCycleCode.stages.eight.ngOnDestroy.ts}}
  </code>
  </pre>
  <div class="note">
  <h6>navigate to another component then check to console</h6>
  </div>
  <hr>
  <h2>ngOnDestroy() use</h2>
  <p>After destroy C1 component , if we don't want to apply method , properties of C1 component in C2 component </p>
  `,
  styles: [
  ]
})
export class HookEightComponent implements OnInit {
  LifeCycleCode = LifeCycleCode;
  constructor() { }

  ngOnInit(): void {
  }
  ngOnDestroy(): void {
    console.log('ngOnDestroy is called -> current component is Destroy');
  }
}
