import { Component, OnInit } from '@angular/core';
import { LifeCycleCode } from 'src/app/angular-dashboard/life-cycle-and-hooks/code/source-code-LifeCycle';

@Component({
  selector: 'app-hook-six',
  template: `
    <h1 style="text-transform: unset;">
    6 . ngAfterViewInit()
    </h1>
    <hr>
    <p>when component is completely initialized and also loaded view part than after work ngAfterViewInit() method</p>
    <pre data-doc="1 name.component.html">
      <code>
      {{LifeCycleCode.stages.six.ngAfterViewInit.ts}}
      </code>
    </pre>
    <div class="note">
    <h5>check to console</h5>
    </div>
  `,
  styles: [
  ]
})
export class HookSixComponent implements OnInit {
  LifeCycleCode = LifeCycleCode;
  constructor() { }

  ngOnInit(): void {
  }
  ngAfterViewInit(): void {
    console.log('ngAfterViewInit has called ');
  }
}
