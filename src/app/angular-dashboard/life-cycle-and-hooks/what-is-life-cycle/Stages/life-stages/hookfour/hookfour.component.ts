import { Component, OnInit } from '@angular/core';
import { LifeCycleCode } from 'src/app/angular-dashboard/life-cycle-and-hooks/code/source-code-LifeCycle';

@Component({
  selector: 'app-hookfour',
  template: `
    <h1 style="text-transform: unset;">
    4 . ngAfterContentInit()
    </h1>
    <hr>
    <p>when any component and directive has been initialized Than work ngAfterContentInit() method</p>
    <p>This method is load before component's content has render in DOM</p>
    <p>ngAfterContentInit() is called after ngOnInit()</p>
    <hr>
    <pre data-doc="1 name.component.ts">
      <code>
      {{ LifeCycleCode.stages.four.ngAfterContentInit.ts}}
      </code>
    </pre>
    <div class="note">
    <h5>check in console</h5>
    </div>
   
  `,
  styles: [
  ]
})
export class HookfourComponent implements OnInit {
  LifeCycleCode = LifeCycleCode
  constructor() { }

  ngOnInit(): void {
  }
  ngAfterContentInit() {
    console.log('ngAfterContentInit() is called');
  }
}
