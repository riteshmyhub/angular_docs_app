import { Component, OnInit } from '@angular/core';
import { LifeCycleCode } from 'src/app/angular-dashboard/life-cycle-and-hooks/code/source-code-LifeCycle';

@Component({
  selector: 'app-hook-three',
  template: `
  <h1 style="text-transform: unset;">
  3 . ngDoCheck()
  </h1>
  <hr>
  <p>When Any changes in Html template of component in that condition work ngDoCheck() mathod.</p>
  <p>this method is detect to any changes html template of component</p> 
  <hr>
   <pre data-doc="1 name.component.html">
      <code>
      {{LifeCycleCode.stages.three.ngDoCheck.html}}
      </code>
    </pre>
    <pre data-doc="2 name.component.ts">
      <code>
      {{LifeCycleCode.stages.three.ngDoCheck.ts}}
      </code>
    </pre>
  <hr>
  <div class="output">
  <div class="p-4">
    <div *ngFor="let item of myObj">
       <h4>{{ item }}</h4>
    </div>
     <button class="btn btn-primary btn-sm" (click)="push_thing()">store data</button>
     <div class="note">
       <h5>check to console</h5>
     </div>
  </div>
  </div>
  `,
  styles: [
  ]
})
export class HookThreeComponent implements OnInit {
  LifeCycleCode =LifeCycleCode;
  constructor() { }

  ngOnInit(): void {
  }
  myObj = ['one', 'two', 'three'];
  push_thing() {
    this.myObj.push('four');
  }
  ngDoCheck(): void {
    console.log('ngDoCheck is called');
  }
}
