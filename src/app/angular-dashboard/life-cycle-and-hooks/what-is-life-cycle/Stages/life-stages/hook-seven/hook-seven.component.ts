import { Component, OnInit } from '@angular/core';
import { LifeCycleCode } from 'src/app/angular-dashboard/life-cycle-and-hooks/code/source-code-LifeCycle';

@Component({
  selector: 'app-hook-seven',
  template: `
  <h1 style="text-transform: unset;">
  7 . ngAfterViewChecked()
  </h1>
  <hr>
  <p>when view is rendar in component so it is check than execute ngAfterViewChecked() method</p>
  <pre data-doc="1 name.component.html">
    <code>
    {{LifeCycleCode.stages.seven.ngAfterViewChecked.ts}}
    </code>
  </pre>
  <div class="note">
  <h6>check to console</h6>
  </div>
  `,
  styles: [
  ]
})
export class HookSevenComponent implements OnInit {
  LifeCycleCode = LifeCycleCode;
  constructor() { }

  ngOnInit(): void {
  }
  ngAfterViewChecked(): void {
    console.log("ngAfterViewChecked is called");
  }
}
