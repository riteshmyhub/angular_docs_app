import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-life-child',
  templateUrl: './life-child.component.html',
  styleUrls: ['./life-child.component.css']
})
export class LifeChildComponent implements OnInit {
  @Input() data: string;
  //ngOnChanges() has Tree property 
  current_data = null;
  previous_data = null;
  constructor() { }
  ngOnChanges(changes) {
    console.log('ngOnChanges is called');
    this.current_data = changes.data.currentValue;
    this.previous_data = changes.data.previousValue;
  }
  ngOnInit(): void {
  }

}
