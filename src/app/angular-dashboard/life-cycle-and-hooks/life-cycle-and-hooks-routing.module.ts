import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LifeCycleAndHooksComponent } from './life-cycle-and-hooks.component';
import { WhatIsLifeCycleComponent } from './what-is-life-cycle/what-is-life-cycle.component';
import { LifeConstructorComponent } from './what-is-life-cycle/life-constructor/life-constructor.component';
import { LifeStagesComponent } from './what-is-life-cycle/Stages/life-stages/life-stages.component';
import { HookOneComponent } from './what-is-life-cycle/Stages/life-stages/hook-one/hook-one.component';
import { HookTwoComponent } from './what-is-life-cycle/Stages/life-stages/hook-two/hook-two.component';
import { HookThreeComponent } from './what-is-life-cycle/Stages/life-stages/hook-three/hook-three.component';
import { HookfourComponent } from './what-is-life-cycle/Stages/life-stages/hookfour/hookfour.component';
import { HookfiveComponent } from './what-is-life-cycle/Stages/life-stages/hookfive/hookfive.component';
import { HookSixComponent } from './what-is-life-cycle/Stages/life-stages/hook-six/hook-six.component';
import { HookSevenComponent } from './what-is-life-cycle/Stages/life-stages/hook-seven/hook-seven.component';
import { HookEightComponent } from './what-is-life-cycle/Stages/life-stages/hook-eight/hook-eight.component';

const routes: Routes = [
  { path: '', component: LifeCycleAndHooksComponent , children:[
    {path:'', component:WhatIsLifeCycleComponent},
    {path:'what-is-LifeCycle', component:WhatIsLifeCycleComponent},
    {path:'Constructor',component:LifeConstructorComponent},
    {path:'',children:[
      {path:'LifeCycle-Stages',component:LifeStagesComponent},
      {path:'ngOnChanges',component:HookOneComponent},
      {path:'ngOnInit',component:HookTwoComponent},
      {path:'ngDoCheck',component:HookThreeComponent},
      {path:'ngAfterContentInit',component:HookfourComponent},
      {path:'ngAfterContentChecked',component:HookfiveComponent},
      {path:'ngAfterViewInit',component:HookSixComponent},
      {path:'ngAfterViewChecked',component:HookSevenComponent},
      {path:'ngOnDestroy',component:HookEightComponent},
    ]}
  ]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LifeCycleAndHooksRoutingModule { }
