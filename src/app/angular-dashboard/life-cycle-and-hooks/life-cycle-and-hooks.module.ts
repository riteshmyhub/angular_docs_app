import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LifeCycleAndHooksRoutingModule } from './life-cycle-and-hooks-routing.module';
import { WhatIsLifeCycleComponent } from './what-is-life-cycle/what-is-life-cycle.component';
import { LifeConstructorComponent } from './what-is-life-cycle/life-constructor/life-constructor.component';
import { LifeStagesComponent } from './what-is-life-cycle/Stages/life-stages/life-stages.component';
import { LifeChildComponent } from './what-is-life-cycle/Stages/life-child/life-child.component';
import { HookOneComponent } from './what-is-life-cycle/Stages/life-stages/hook-one/hook-one.component';
import { HookTwoComponent } from './what-is-life-cycle/Stages/life-stages/hook-two/hook-two.component';
import { HookThreeComponent } from './what-is-life-cycle/Stages/life-stages/hook-three/hook-three.component';
import { HookfourComponent } from './what-is-life-cycle/Stages/life-stages/hookfour/hookfour.component';
import { HookfiveComponent } from './what-is-life-cycle/Stages/life-stages/hookfive/hookfive.component';
import { HookSixComponent } from './what-is-life-cycle/Stages/life-stages/hook-six/hook-six.component';
import { HookSevenComponent } from './what-is-life-cycle/Stages/life-stages/hook-seven/hook-seven.component';
import { HookEightComponent } from './what-is-life-cycle/Stages/life-stages/hook-eight/hook-eight.component';


@NgModule({
  declarations: [WhatIsLifeCycleComponent, LifeConstructorComponent, LifeStagesComponent, LifeChildComponent, HookOneComponent, HookTwoComponent, HookThreeComponent, HookfourComponent, HookfiveComponent, HookSixComponent, HookSevenComponent, HookEightComponent],
  imports: [
    CommonModule,
    LifeCycleAndHooksRoutingModule
  ]
})
export class LifeCycleAndHooksModule { }
