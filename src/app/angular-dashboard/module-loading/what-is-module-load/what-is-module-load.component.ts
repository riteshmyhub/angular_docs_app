import { Component, OnInit } from '@angular/core';
import { ModuleLoadCode } from '../code/source-code-module';

@Component({
  selector: 'app-what-is-module-load',
  templateUrl: './what-is-module-load.component.html',
  styleUrls: ['./what-is-module-load.component.css']
})
export class WhatIsModuleLoadComponent implements OnInit {
  ModuleLoadCode =ModuleLoadCode;
  constructor() { }

  ngOnInit(): void {
  }

}
