export const ModuleLoadList = [
    { id: 1, point: 'What is Module Load', path: 'What-Is-Module-Load' },
    { id: 1, point: 'Feature Modules', path: 'Feature-Modules' },
]
export const ModuleLoadCode = {
    moduleLoading:{
        appMode:`
        import { StudentModule } from "./student/student.module";
        @NgModule({
          declarations: [AppComponent],
          imports: [
          BrowserModule,
          AppRoutingModule,
          StudentModule,
          ]
         })`,
         lazyL:{
            syntax:`
            import { StudentModule } from "./student/student.module";

              const routes: Routes =[
              { path: "student", loadChildren: () => import('./student/student.module').then(m => m.StudentModule) }
                   //{ other all feature modules },
               ];`,
             html:`
             <a [routerLink]="['/student']">student scetion</a>
             <router-outlet></router-outlet>`
         },
         preLoading:{
           routeMode:`
           import { StudentModule } from "./student/student.module";

           const routes: Routes =[
            { path: "student", loadChildren: () => import('./student/student.module').then(m => m.StudentModule) }
                 //{ other all feature modules },
             ];
             @NgModule({
                imports: [RouterModule.forRoot(routes, {
                  preloadingStrategy: PreloadAllModules;
                })],
                exports: [RouterModule]
              })`,
         },
        //  sdfsdf
        CustompreLoading:{
            service:`
             import { Injectable } from '@angular/core';
             import { PreloadingStrategy, Route } from '@angular/ router';
             import { Observable, of } from 'rxjs';

             @Injectable({
               providedIn: 'root'
             })
            export class CustomPreloadingService implements PreloadingStrategy {
              preload(route: Route, fn: () => Observable<any>): Observable<any> {
                if (route.data && route.data['preload']) {
                  return fn();
                } else {
                  return of(null)
                }
              }
              constructor() { }
            }`,
            routeMode:`
            const routes: Routes = [
                { path: "student", loadChildren: () => import('./student/student.module').then(m => m.StudentModule) },

                { path: "scholarship", data: { preload: true }, loadChildren: () => import('./scholarship/scholarship.module').then(m => m.ScholarshipModule) },
               
                { path: "admission", data: { preload: true }, loadChildren: () => import('./admission/admission.module').then(m => m.AdmissionModule) },
              ];
              @NgModule({
                imports: [RouterModule.forRoot(routes, {
                  preloadingStrategy: CustomPreloadingService
                })],
                providers: [CustomPreloadingService]
              })`,
        },
    },
}