import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ModuleLoadingComponent } from './module-loading.component';
import { WhatIsModuleLoadComponent } from './what-is-module-load/what-is-module-load.component';
import { FeatureModulesComponent } from './feature-modules/feature-modules.component';
import { EagerLoadingComponent } from './types/eager-loading/eager-loading.component';
import { LazyLoadingComponent } from './types/lazy-loading/lazy-loading.component';
import { PreLoadingComponent } from './types/pre-loading/pre-loading.component';
import { CustomPreLoadingComponent } from './types/custom-pre-loading/custom-pre-loading.component';

const routes: Routes = [
  {path:'',component:ModuleLoadingComponent,children:[
    {path:'',component:WhatIsModuleLoadComponent},
     {path:'',children:[
      {path:'What-Is-Module-Load',component:WhatIsModuleLoadComponent},
      {path:'Eager-Loading',component:EagerLoadingComponent},
      {path:'Lazy-Loading',component:LazyLoadingComponent},
      {path:'Pre-Loading',component:PreLoadingComponent},
      {path:'Custom-Pre-Loading',component:CustomPreLoadingComponent}, 
    ]},
    {path:'Feature-Modules',component:FeatureModulesComponent},
  ]},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ModuleLoadingRoutingModule { }
