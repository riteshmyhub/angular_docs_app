import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ModuleLoadingRoutingModule } from './module-loading-routing.module';
import { WhatIsModuleLoadComponent } from './what-is-module-load/what-is-module-load.component';
import { FeatureModulesComponent } from './feature-modules/feature-modules.component';
import { EagerLoadingComponent } from './types/eager-loading/eager-loading.component';
import { LazyLoadingComponent } from './types/lazy-loading/lazy-loading.component';
import { PreLoadingComponent } from './types/pre-loading/pre-loading.component';
import { CustomPreLoadingComponent } from './types/custom-pre-loading/custom-pre-loading.component';


@NgModule({
  declarations: [WhatIsModuleLoadComponent, FeatureModulesComponent, EagerLoadingComponent, LazyLoadingComponent, PreLoadingComponent, CustomPreLoadingComponent],
  imports: [
    CommonModule,
    ModuleLoadingRoutingModule
  ]
})
export class ModuleLoadingModule { }
