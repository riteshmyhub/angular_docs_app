import { Component, OnInit } from '@angular/core';
import { ModuleLoadCode } from '../../code/source-code-module';

@Component({
  selector: 'app-eager-loading',
  templateUrl: './eager-loading.component.html',
  styleUrls: ['./eager-loading.component.css']
})
export class EagerLoadingComponent implements OnInit {
  ModuleLoadCode =ModuleLoadCode;
  constructor() { }

  ngOnInit(): void {
  }

}
