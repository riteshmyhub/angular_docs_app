import { Component, OnInit } from '@angular/core';
import { ModuleLoadCode } from '../../code/source-code-module';

@Component({
  selector: 'app-pre-loading',
  templateUrl: './pre-loading.component.html',
  styleUrls: ['./pre-loading.component.css']
})
export class PreLoadingComponent implements OnInit {
  ModuleLoadCode=ModuleLoadCode;
  constructor() { }

  ngOnInit(): void {
  }

}
