import { Component, OnInit } from '@angular/core';
import { ModuleLoadCode } from '../../code/source-code-module';

@Component({
  selector: 'app-lazy-loading',
  templateUrl: './lazy-loading.component.html',
  styleUrls: ['./lazy-loading.component.css']
})
export class LazyLoadingComponent implements OnInit {
  ModuleLoadCode =ModuleLoadCode;
  constructor() { }

  ngOnInit(): void {
  }

}
