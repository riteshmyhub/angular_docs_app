import { Component, OnInit } from '@angular/core';
import { ModuleLoadCode } from '../../code/source-code-module';

@Component({
  selector: 'app-custom-pre-loading',
  templateUrl: './custom-pre-loading.component.html',
  styleUrls: ['./custom-pre-loading.component.css']
})
export class CustomPreLoadingComponent implements OnInit {
  ModuleLoadCode=ModuleLoadCode
  constructor() { }

  ngOnInit(): void {
  }

}
