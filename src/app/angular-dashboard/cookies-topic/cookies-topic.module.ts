import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CookiesTopicRoutingModule } from './cookies-topic-routing.module';
import { WhatIsCookiesComponent } from './what-is-cookies/what-is-cookies.component';
import { SetupCookiesComponent } from './setup-cookies/setup-cookies.component';
import { CookieService} from 'ngx-cookie-service';
import { from } from 'rxjs';

@NgModule({
  declarations: [WhatIsCookiesComponent, SetupCookiesComponent],
  imports: [
    CommonModule,
    CookiesTopicRoutingModule
  ],
  providers:[CookieService]
})
export class CookiesTopicModule { }
