import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WhatIsCookiesComponent } from './what-is-cookies.component';

describe('WhatIsCookiesComponent', () => {
  let component: WhatIsCookiesComponent;
  let fixture: ComponentFixture<WhatIsCookiesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WhatIsCookiesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WhatIsCookiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
