import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { cookiesCode } from '../code/source-code-cookies';

@Component({
  selector: 'app-setup-cookies',
  templateUrl: './setup-cookies.component.html',
  styleUrls: ['./setup-cookies.component.css']
})
export class SetupCookiesComponent implements OnInit {
  cookiesCode=cookiesCode;
  constructor(private cookie: CookieService) { }

  ngOnInit(): void {
  }
  GetCookieData;
  // set Cookies Data
  cookie_set() {
    this.cookie.set('user', 'alex');
  }
  // get Cookies Data
  cookie_get() {
    this.cookie.set('user', 'alex');
    this.GetCookieData = this.cookie.get('user');
  }
  // delete Cookies Data
  cookie_delete() {
    this.cookie.set('user', 'alex');
    this.GetCookieData = this.cookie.delete('user');
  }
}
