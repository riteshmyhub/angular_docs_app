import { Component, OnInit } from '@angular/core';
import { cookiesList } from './code/source-code-cookies'
@Component({
  selector: 'app-cookies-topic',
  templateUrl: './cookies-topic.component.html',
  styleUrls: ['./cookies-topic.component.css']
})
export class CookiesTopicComponent implements OnInit {
  cookiesList = cookiesList;
  constructor() { }

  ngOnInit(): void {
  }

}
