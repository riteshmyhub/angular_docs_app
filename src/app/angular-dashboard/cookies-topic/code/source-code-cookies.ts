export const cookiesList = [
    { id: 1, point: 'What Is Cookies', path: 'What-Is-Cookies' },
    { id: 1, point: 'Setup Cookies', path: 'SetupCookies' },
]
export const cookiesCode = {
    appMode: `
    import { CookieService} from 'ngx-cookie-service';

    @NgModule({
        declarations: [],
        imports: [],
        providers:[CookieService]
    })`,
    ts: `
    GetCookieData;

    constructor(private cookie: CookieService) { }

    // set Cookies Data
    cookie_set() {
      this.cookie.set('user', 'alex');
    }
    // get Cookies Data
    cookie_get() {
      this.cookie.set('user', 'alex');
      this.GetCookieData = this.cookie.get('user');
    }
    // delete Cookies Data
    cookie_delete() {
      this.cookie.set('user', 'alex');
      this.GetCookieData = this.cookie.delete('user');
    }`,
    html: `
        <h4>This is Cookies data : {{GetCookieData}}</h4>
        <button (click)="cookie_set()">set cookie</button>&nbsp;
        <button (click)="cookie_get()"> get cookie</button>&nbsp;
        <button (click)="cookie_delete()"> delete cookie</button>`,
}