import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CookiesTopicComponent } from './cookies-topic.component';
import { WhatIsCookiesComponent } from './what-is-cookies/what-is-cookies.component';
import { SetupCookiesComponent } from './setup-cookies/setup-cookies.component';

const routes: Routes = [
  { path: '', component: CookiesTopicComponent ,children:[
    {path:'',component:WhatIsCookiesComponent},
    {path:'What-Is-Cookies',component:WhatIsCookiesComponent},
    {path:'SetupCookies',component:SetupCookiesComponent},
  ]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CookiesTopicRoutingModule { }
