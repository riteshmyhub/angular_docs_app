import { Component, OnInit } from '@angular/core';
import { DI_List } from './code/source-code-Di';
@Component({
  selector: 'app-dependency-injection',
  templateUrl: './dependency-injection.component.html',
  styleUrls: ['./dependency-injection.component.css']
})
export class DependencyInjectionComponent implements OnInit {
  DI_List = DI_List;
  constructor() { }

  ngOnInit(): void {
  }

}
