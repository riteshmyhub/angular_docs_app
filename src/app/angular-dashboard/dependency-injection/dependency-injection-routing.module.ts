import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { WhatIsDiComponent } from './what-is-di/what-is-di.component';
import { DependencyInjectionComponent } from './dependency-injection.component';
import { DiServiceComponent } from './di-service/di-service.component';

const routes: Routes = [
  {path:'',component:DependencyInjectionComponent , children:[
    {path:'',component:WhatIsDiComponent },
    {path:'What-Is-DI',component:WhatIsDiComponent },
    {path:'Service',component:DiServiceComponent}
  ]},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DependencyInjectionRoutingModule { }
