import { Component, OnInit } from '@angular/core';
import { DiService } from './service/di.service';
import { DI_Code } from '../code/source-code-Di';
@Component({
  selector: 'app-di-service',
  templateUrl: './di-service.component.html',
  styleUrls: ['./di-service.component.css'],
  providers: [DiService]
})
export class DiServiceComponent implements OnInit {
  DI_Code = DI_Code;

  text_data: string;
  constructor(private sevice: DiService) { }

  ngOnInit(): void {
    this.text_data = this.sevice.s_method()
  }

}
