export const DI_List = [
    { id: 1, point: 'What Is Dependency Injection', path: 'What-Is-DI' },
    { id: 1, point: 'Service', path: 'Service' },
]
export const DI_Code = {
    serviceTs: `
    import { Injectable } from '@angular/core';

    @Injectable({
      providedIn: 'root'
    })
    export class DiService {

    constructor() { }

     s_method() {
       return 'this data from Service'
     }
    }`,
   ts:`
      import { Component, OnInit } from '@angular/core';
      import { DiService } from './service/di.service';

      @Component({
        selector: 'app-di-service',
        templateUrl: './di-service.component.html',
        styleUrls: ['./di-service.component.css'],
        providers: [DiService]
      })
      
      export class DiServiceComponent implements OnInit {
        text_data: string;
        constructor(private sevice: DiService) { }

        ngOnInit(): void {
          this.text_data = this.sevice.s_method()
        }
      }`,
      html:`
      <h4>{{text_data}}</h4>`,
}