import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DependencyInjectionRoutingModule } from './dependency-injection-routing.module';
import { WhatIsDiComponent } from './what-is-di/what-is-di.component';
import { DiServiceComponent } from './di-service/di-service.component';


@NgModule({
  declarations: [WhatIsDiComponent, DiServiceComponent],
  imports: [
    CommonModule,
    DependencyInjectionRoutingModule
  ]
})
export class DependencyInjectionModule { }
