import { Component, OnInit } from '@angular/core';
import { DI_Code } from '../code/source-code-Di';

@Component({
  selector: 'app-what-is-di',
  templateUrl: './what-is-di.component.html',
  styleUrls: ['./what-is-di.component.css']
})
export class WhatIsDiComponent implements OnInit {
  DI_Code = DI_Code;
  constructor() { }

  ngOnInit(): void {
  }

}
