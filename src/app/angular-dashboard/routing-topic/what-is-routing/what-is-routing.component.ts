import { Component, OnInit } from '@angular/core';
import { codeRouting } from '../code/source-code-routing';

@Component({
  selector: 'app-what-is-routing',
  templateUrl: './what-is-routing.component.html',
  styleUrls: ['./what-is-routing.component.css']
})
export class WhatIsRoutingComponent implements OnInit {
  codeRouting=codeRouting;
  constructor() { }

  ngOnInit(): void {
  }

}
