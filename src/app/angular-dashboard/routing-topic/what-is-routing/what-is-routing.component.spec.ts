import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WhatIsRoutingComponent } from './what-is-routing.component';

describe('WhatIsRoutingComponent', () => {
  let component: WhatIsRoutingComponent;
  let fixture: ComponentFixture<WhatIsRoutingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WhatIsRoutingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WhatIsRoutingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
