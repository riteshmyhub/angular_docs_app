import { Component, OnInit } from '@angular/core';
import { codeRouting } from '../../code/source-code-routing';

@Component({
  selector: 'app-router-outlet-topic',
  templateUrl: './router-outlet-topic.component.html',
  styleUrls: ['./router-outlet-topic.component.css']
})
export class RouterOutletTopicComponent implements OnInit {
  codeRouting=codeRouting;
  constructor() { }

  ngOnInit(): void {
  }

}
