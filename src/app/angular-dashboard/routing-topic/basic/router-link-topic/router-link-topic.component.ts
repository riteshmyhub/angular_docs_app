import { Component, OnInit } from '@angular/core';
import { codeRouting } from '../../code/source-code-routing';

@Component({
  selector: 'app-router-link-topic',
  templateUrl: './router-link-topic.component.html',
  styleUrls: ['./router-link-topic.component.css']
})
export class RouterLinkTopicComponent implements OnInit {
  codeRouting=codeRouting;
  constructor() { }

  ngOnInit(): void {
  }

}
