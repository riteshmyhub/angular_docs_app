import { Component, OnInit } from '@angular/core';
import { codeRouting } from '../code/source-code-routing';

@Component({
  selector: 'app-auxiliary',
  templateUrl: './auxiliary.component.html',
  styleUrls: ['./auxiliary.component.css']
})
export class AuxiliaryComponent implements OnInit {
  codeRouting=codeRouting;
  constructor() { }

  ngOnInit(): void {
  }

}
