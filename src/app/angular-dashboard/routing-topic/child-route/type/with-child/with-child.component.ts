import { Component, OnInit } from '@angular/core';
import { codeRouting } from '../../../code/source-code-routing';

@Component({
  selector: 'app-with-child',
  templateUrl: './with-child.component.html',
  styleUrls: ['./with-child.component.css']
})
export class WithChildComponent implements OnInit {
  codeRouting = codeRouting;
  constructor() { }

  ngOnInit(): void {
  }

}
