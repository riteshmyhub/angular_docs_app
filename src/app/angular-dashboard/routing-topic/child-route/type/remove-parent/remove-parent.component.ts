import { Component, OnInit } from '@angular/core';
import { codeRouting } from '../../../code/source-code-routing';

@Component({
  selector: 'app-remove-parent',
  templateUrl: './remove-parent.component.html',
  styleUrls: ['./remove-parent.component.css']
})
export class RemoveParentComponent implements OnInit {
  codeRouting=codeRouting;
  constructor() { }

  ngOnInit(): void {
  }

}
