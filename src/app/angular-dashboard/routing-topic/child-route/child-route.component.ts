import { Component, OnInit } from '@angular/core';
import { codeRouting } from '../code/source-code-routing';

@Component({
  selector: 'app-child-route',
  templateUrl: './child-route.component.html',
  styleUrls: ['./child-route.component.css']
})
export class ChildRouteComponent implements OnInit {
  codeRouting=codeRouting;
  constructor() { }

  ngOnInit(): void {
  }

}
