import { Component, OnInit } from '@angular/core';
import { codeRouting } from '../code/source-code-routing';

@Component({
  selector: 'app-how-to-do-routing',
  templateUrl: './how-to-do-routing.component.html',
  styleUrls: ['./how-to-do-routing.component.css']
})
export class HowToDoRoutingComponent implements OnInit {
  codeRouting=codeRouting;
  constructor() { }

  ngOnInit(): void {
  }

}
