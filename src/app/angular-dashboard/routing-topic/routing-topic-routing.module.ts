import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RoutingTopicComponent } from './routing-topic.component';
import { WhatIsRoutingComponent } from './what-is-routing/what-is-routing.component';
import { AuxiliaryComponent } from './auxiliary/auxiliary.component';
import { COneComponent } from './auxiliary/cone/cone.component';
import {RouterTopicComponent} from './basic/router-topic/router-topic.component';
import {RouterOutletTopicComponent} from './basic/router-outlet-topic/router-outlet-topic.component';
import {RouterLinkTopicComponent} from './basic/router-link-topic/router-link-topic.component';
import {HowToDoRoutingComponent} from './how-to-do-routing/how-to-do-routing.component';
import {EmptyPathComponent} from './empty-path/empty-path.component';
import {RedirectiveRouterComponent} from './redirective-router/redirective-router.component';
import {WildCardComponent} from './wild-card/wild-card.component';
import {ChildRouteComponent} from './child-route/child-route.component';
import {WithChildComponent} from './child-route/type/with-child/with-child.component';
import {RemoveParentComponent} from './child-route/type/remove-parent/remove-parent.component';
import { from } from 'rxjs';

const routes: Routes = [
  {path:'',component:RoutingTopicComponent,children:[
    {path:'',component:WhatIsRoutingComponent},
    {path:'what-is-routing',component:WhatIsRoutingComponent},
    {path:'Auxiliary-Routes',component:AuxiliaryComponent,children:[
      {path:'one',outlet:'Alex',component:COneComponent},
    ]},
    {path:'router',component:RouterTopicComponent},
    {path:'router-outlet',component:RouterOutletTopicComponent},
    {path:'router-link',component:RouterLinkTopicComponent},
    {path:'how-to-do-routing',component:HowToDoRoutingComponent},
    {path:'empty-path',component:EmptyPathComponent},
    {path:'Redirect',component:RedirectiveRouterComponent},
    {path:'wild-card',component:WildCardComponent},
    {path:'',children:[
      {path:'child-route',component:ChildRouteComponent},
      {path:'with-child',component:WithChildComponent},
      {path:'remove-parent',component:RemoveParentComponent},
    ]}
  ]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RoutingTopicRoutingModule { }
