import { Component, OnInit } from '@angular/core';
import { codeRouting } from '../code/source-code-routing';

@Component({
  selector: 'app-empty-path',
  templateUrl: './empty-path.component.html',
  styleUrls: ['./empty-path.component.css']
})
export class EmptyPathComponent implements OnInit {
  codeRouting=codeRouting;
  constructor() { }

  ngOnInit(): void {
  }

}
