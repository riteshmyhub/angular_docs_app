import { Component, OnInit } from '@angular/core';
import { codeRouting } from '../code/source-code-routing';

@Component({
  selector: 'app-redirective-router',
  templateUrl: './redirective-router.component.html',
  styleUrls: ['./redirective-router.component.css']
})
export class RedirectiveRouterComponent implements OnInit {
  codeRouting=codeRouting;
  constructor() { }

  ngOnInit(): void {
  }

}
