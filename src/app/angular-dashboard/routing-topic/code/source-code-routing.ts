export const RoutingList = [
    { id: 1, point: 'what is routing', path: 'what-is-routing' },
    { id: 2, point: 'router', path: 'router' },
    { id: 3, point: 'router outlet', path: 'router-outlet' },
    { id: 4, point: 'router link', path: 'router-link' },
    { id: 5, point: 'how to do routing', path: 'how-to-do-routing' },
    { id: 6, point: 'empty path', path: 'empty-path' },
    { id: 7, point: 'redirective router', path: 'Redirect' },
    { id: 8, point: 'wild card router', path: 'wild-card' },
    { id: 9, point: 'child route', path: 'child-route' },
    { id: 9, point: 'Auxiliary Routes', path: 'Auxiliary-Routes' },
]
export const codeRouting = {
    outlet:{
        syntax:`
        <router-outlet></router-outlet>`,
    },
    routerLink:{
        syntax:`
        <a [routerLink]="['/student']"> Mobile </a>
         <router-outlet></router-outlet>`,
    },
    howToRouting:{
        Static:{
            ts:`
            const router:Routers=[
                { path:"student" , component :StudentComponent },
            ];`,
            html:`
            <a [routerLink]="['/student']"> Mobile </a>

            <router-outlet></router-outlet>`,
        },
        dynamic:{
          ts:`
          import { Router} from "@angular/router";
         export class AppComponent {
             constructor(private router:Router) {}
             method(){
                 this.router.navigate(['/student']);
             }
           }`,
          html:`
          <button (click)= "method()"> Mobile </button>

          <router-outlet></router-outlet>`,
        },
    },
    empty:{
        syntax:`
        {path: " " , component: name of componet}`,
        Ex:`
        const routes : Router = [
            {path:"" , component : StudentComponent},
            {---------import all component---------},
        ];`,
    },
    redirect:{
        syntax:`
        const router:Routers=[
            {path:"" , redirectTo:"path name of component" ,pathMatch:"full"}
        ];`,
        EX:`
        const router:Routers=[
            {path:"" , redirectTo:"product" ,pathMatch:"full"},
            {path:"product" , component: ProductComponent },
        ];`,
    },
    wildCard:{
        syntax:`
        const router:Routers=[
            {path: "**", component: Page-not-foundComponent },
        ];`,
    },
    childRoute:{
          withParent:{
            ts:`
            const router:Routers=[

                {path: "product", component: ProductComponent,
            
                 children:[
                     {path: "mobile", component: MobileComponent},
                     {path: "tv", component: TvComponent},
                     {path: "leptop", component: LeptopComponent},
                    ];
                },
            
            ];`,
            html:`
            <h1>products</h1>

            <a [routerLink]="['./mobile']"> Mobile </a>
            <a [routerLink]="['./tv']"> tv </a>
            <a [routerLink]="['./leptop']"> laptop </a>
   
            <router-outlet></router-outlet>`,
          },
          removeParent:{
            ts:`
            const router:Routers=[

                {path: "product", 
            
                    children:[
                     {path: "" ,component: ProductComponent,}//parent component here.
            
                     {path: "mobile", component: MobileComponent},
                     {path: "tv", component: TvComponent},
                     {path: "leptop", component: LeptopComponent},
                    ];
                },
            
            ];`,
            html:`
            <h1>products</h1>

         <a [routerLink]="['./mobile']"> Mobile </a>
         <a [routerLink]="['./tv']"> tv </a>
         <a [routerLink]="['./leptop']"> laptop </a>

         <router-outlet></router-outlet>`,
        },
    },
    // Auxiliary topic
    Auxiliary: {
        html: `
        <a class="btn btn-dark"  [routerLink]="[{
            outlets:{Alex:['one']}
        }]">click here</a> 
        <router-outlet></router-outlet>
        <router-outlet name='Alex'></router-outlet>`,
        routing_ts: `
        const routes: Routes = [
            {path:'Auxiliary-Routes',component:AuxiliaryComponent,children:[
                {path:'one',outlet:'Alex',component:COneComponent},
              ]}
        ]`,
    }
}