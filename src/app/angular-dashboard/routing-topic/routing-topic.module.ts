import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RoutingTopicRoutingModule } from './routing-topic-routing.module';
import { WhatIsRoutingComponent } from './what-is-routing/what-is-routing.component';
import { AuxiliaryComponent } from './auxiliary/auxiliary.component';
import { COneComponent } from './auxiliary/cone/cone.component';
import { RouterTopicComponent } from './basic/router-topic/router-topic.component';
import { RouterOutletTopicComponent } from './basic/router-outlet-topic/router-outlet-topic.component';
import { RouterLinkTopicComponent } from './basic/router-link-topic/router-link-topic.component';
import { HowToDoRoutingComponent } from './how-to-do-routing/how-to-do-routing.component';
import { EmptyPathComponent } from './empty-path/empty-path.component';
import { RedirectiveRouterComponent } from './redirective-router/redirective-router.component';
import { WildCardComponent } from './wild-card/wild-card.component';
import { ChildRouteComponent } from './child-route/child-route.component';
import { WithChildComponent } from './child-route/type/with-child/with-child.component';
import { RemoveParentComponent } from './child-route/type/remove-parent/remove-parent.component';


@NgModule({
  declarations: [WhatIsRoutingComponent, AuxiliaryComponent, COneComponent, RouterTopicComponent, RouterOutletTopicComponent, RouterLinkTopicComponent, HowToDoRoutingComponent, EmptyPathComponent, RedirectiveRouterComponent, WildCardComponent, ChildRouteComponent, WithChildComponent, RemoveParentComponent],
  imports: [
    CommonModule,
    RoutingTopicRoutingModule
  ]
})
export class RoutingTopicModule { }
