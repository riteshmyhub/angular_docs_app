import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RoutingTopicComponent } from './routing-topic.component';

describe('RoutingTopicComponent', () => {
  let component: RoutingTopicComponent;
  let fixture: ComponentFixture<RoutingTopicComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RoutingTopicComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RoutingTopicComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
