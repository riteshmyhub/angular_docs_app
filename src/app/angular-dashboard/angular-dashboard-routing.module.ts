import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AngularDashboardModule } from './angular-dashboard.module';
import { AngularDashboardComponent } from './angular-dashboard.component';
import { IntroductionComponent } from './introduction/introduction.component';

import { from } from 'rxjs';
// ModuleTopicModule
const routes: Routes = [
  {
    path: "", component: AngularDashboardComponent, children: [
      //___________________all Topic here_________________

      // Introduction
      { path: "", loadChildren: () => import('./introduction/introduction.module').then(m => m.IntroductionModule) },
      // ModuleTopic
      { path: "module", loadChildren: () => import('./module-topic/module-topic.module').then(m => m.ModuleTopicModule) },
      // directive
      { path: "directive", loadChildren: () => import('./directive/directive.module').then(m => m.DirectiveModule) },
      // DecoratorsTopic
      { path: "decorators", loadChildren: () => import('./decorators-topic/decorators-topic.module').then(m => m.DecoratorsTopicModule) },
      //component
      { path: "component", loadChildren: () => import('./components/components.module').then(m => m.ComponentsModule) },
      // data binding
      { path: "data_binding", loadChildren: () => import('./data-binding/data-binding.module').then(m => m.DataBindingModule) },
      // pipes 
      { path: "pipes", loadChildren: () => import('./pipes-topic/pipes-topic.module').then(m => m.PipesTopicModule) },
      // Session
      { path: "Session", loadChildren: () => import('./session-management/session-management.module').then(m => m.SessionManagementModule) },
      // Routing
      { path: "Routing", loadChildren: () => import('./routing-topic/routing-topic.module').then(m => m.RoutingTopicModule) },
      // form
      { path: "Form", loadChildren: () => import('./form/form.module').then(m => m.FormModule) },
      // Form Validation
      { path: "FormValidation", loadChildren: () => import('./form-validation/form-validation.module').then(m => m.FormValidationModule) },
      // Desugars Microsyntax
      { path: "Desugars-Micro-syntax", loadChildren: () => import('./desugars-microsyntax/desugars-microsyntax.module').then(m => m.DesugarsMicrosyntaxModule) },
      // Component Interaction ComponentInteractionModule
      { path: "component-interaction", loadChildren: () => import('./component-interaction/component-interaction.module').then(m => m.ComponentInteractionModule) },
      // module Loading
      { path: "Module-Loading", loadChildren: () => import('./module-loading/module-loading.module').then(m => m.ModuleLoadingModule) },
      // module Loading
      { path: "Observables_Rxjs", loadChildren: () => import('./observables-and-rxjs/observables-and-rxjs.module').then(m => m.ObservablesAndRxjsModule) },
      // HttpClient
      { path: "HttpClient", loadChildren: () => import('./http-client-topic/http-client-topic.module').then(m => m.HttpClientTopicModule) },
      // Cookies
      { path: "Cookies", loadChildren: () => import('./cookies-topic/cookies-topic.module').then(m => m.CookiesTopicModule) },
      // Life Cycle Hooks
      { path: "Life-Cycle-Hooks", loadChildren: () => import('./life-cycle-and-hooks/life-cycle-and-hooks.module').then(m => m.LifeCycleAndHooksModule) },
      // Dependency Injection DependencyInjectionModule
      { path: "Dependency-injection", loadChildren: () => import('./dependency-injection/dependency-injection.module').then(m => m.DependencyInjectionModule) },

    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AngularDashboardRoutingModule { }
